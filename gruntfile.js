var grunt = require('grunt');

grunt.initConfig({
    
    pkg: grunt.file.readJSON('package.json'),
    
    express: {
        options: {
            // Override defaults here 
        },
        dev: {
            options: {
                script: 'server.js'
            }
        },
    },
    
    less: {
        development: {
            options: {
                paths: ["app/client"],
                yuicompress: true
            },
            files: {
                "app/client/index.css": "app/client/index.less"
            }
        }
    },
    
    karma: {
        unit: {
            configFile: 'karma.config.js',
            singleRun: true,
        }
    },
    
    watch: {
        css: {
            files: ["app/client/index.less"],
            tasks: ["less"],
        },
        javascript: {
            files: ['app/**/*.js']
        },
    },
    execute: {
        target_1: {
            src: ['dev/route-reader.js']
        },
        target_2: {
            src: ['dev/sitemap-generator.js']
        }
    }
});

grunt.loadNpmTasks('grunt-execute');
grunt.loadNpmTasks('grunt-express-server');
grunt.loadNpmTasks('grunt-contrib-less');
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-karma');
grunt.loadNpmTasks('grunt-shell');


grunt.registerTask('default', ['express', 'less', 'watch']);
grunt.registerTask('test', ['karma']);
grunt.registerTask('exe', ['execute:target_1']);
grunt.registerTask('generate-map', ['execute:target_2']);
grunt.registerTask('make-live', ['express']);