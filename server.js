/*eslint-disable no-unused-params, no-undef-expression*/
var express = require('express');
var path = require('path');
var fs = require('fs');
var app = express();
var bodyParser = require('body-parser');

// mongoDB 
var MongoClient = require('mongodb').MongoClient, 
    assert = require('assert');
// connection URL
var url = 'mongodb://max:hike-uncap-dry-2@ds243055.mlab.com:43055/images';

// use 
app.use(require('prerender-node').set('prerenderToken', 'OlOX0mMadcvHhPscrHKp'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use('/paths', express.static(__dirname + '/app/client/paths'));
app.use('/app', express.static(__dirname + '/app'));

// API
app.get('/sitemap.xml', function(req, res) {
    res.senddile(path.join(__dirname, 'sitemap.xml'));
});

// Handle all url routes with angularJS routeprovider
app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname, '/app/client', 'index.html'));
});
app.get('/get-image', function(req, res) {
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        db.collection("images").find({}, {[req.query.uuid]: true, _id: false}).toArray(function(err, result) {
            if (!err) {
                result.forEach(function(value) {
                    if(Object.keys(value).length > 0) {
                        res.send(value);
                        return;
                    }
                });
            }
        });
        db.close();
    });
});
app.post('/write', function(req, res) {
    //write new file
    fs.writeFile(req.body.filePath, req.body.template, null, function() {
        //return promise as true
        res.send(true);
    });
});
app.post('/save-new-image', function(req, res) {
    //connect MongoDB
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        //insert the key value for the image
        db.collection('images').insertOne(req.body, function(err, result) {
            if (!err) {
                //return result
                res.send(result);
            }
        });
        db.close();
    });
});
app.patch('/remove-image', function(req, res) {
    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
         db.collection("images").deleteOne(req.body, function(err) {
            if (!err) {
                res.send(true);
            }
        });
        db.close();
    });
});


// listen port
app.listen(process.env.PORT || 3000, function () {
    console.log(process.env.PORT);
    console.log(`app listening on ${process.env.PORT || 3000}`);
});

