angular.module('RevisionApp')
.directive('sidebarOption', function($document, $location) {
    return {
        link: function(scope, element) {
            
            element.on('click', function() {
                element.addClass('selected');
            });
            
            scope.$watch(function() {
                return $location.url();
            }, function(val) {
                element.removeClass('selected');
                var base = val.split('/')[1];
                base = base.replaceAll('-', ' ');
                
                if (base === element.html().toLowerCase()) {
                    element.addClass('selected');
                }
            });
            
            
            String.prototype.replaceAll = function(search, replacement) {
                var target = this;
                return target.split(search).join(replacement);
            };
        }
    };
});