angular.module('RevisionApp')
.controller('sidenavCtrl', function($scope, $location, $mdSidenav) {
    
    $scope.margin = false;
    
    $scope.optionSelect = function(event) {
        //some stuff to convert spaces into hyphens
        var str = event.target.innerHTML.toLowerCase().replaceAll(' ', '-');
        str.toLowerCase();
        $location.url('/' + str);
        $mdSidenav('left').close();
    };
    
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };
    
})