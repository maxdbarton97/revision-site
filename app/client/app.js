angular.module('RevisionApp', ['ng','ngRoute','ngMaterial', 'ngDraggable', 'ngFileUpload', "uuid"])
    
    //theming
    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
        .primaryPalette('grey', {
            'hue-1': '900',
        })
        .accentPalette('red');
    })
    //enable html5 mode
    .config(function($locationProvider) {
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
    })
    //routing
    .config(function($routeProvider) {
    $routeProvider
//dynamically added routes   
.when('/contact-us', {
templateUrl : 'paths/contact-us/contact-us.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-programming/programming/string-handling-operations', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-programming/programming/string-handling-operations/string-handling-operations.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-programming/programming/boolean-operations', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-programming/programming/boolean-operations/boolean-operations.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-programming/programming/relational-operations', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-programming/programming/relational-operations/relational-operations.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-programming/programming/arithmetic-operations', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-programming/programming/arithmetic-operations/arithmetic-operations.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-programming/programming/programming-concepts', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-programming/programming/programming-concepts/programming-concepts.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-programming/programming/data-types', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-programming/programming/data-types/data-types.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-programming/programming-test', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-programming/programming-test/programming-test.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-data-representation/binary-number-system', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-data-representation/binary-number-system/binary-number-system.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-systems/types-of-translator', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-systems/types-of-translator/types-of-translator.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-algorithms/optimisation-algorithms', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-algorithms/optimisation-algorithms/optimisation-algorithms.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-data-structures/data-structures-and-abstract-data-types', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-data-structures/data-structures-and-abstract-data-types/data-structures-and-abstract-data-types.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-communication-and-networking/networking', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-communication-and-networking/networking/networking.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-communication-and-networking/communication', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-communication-and-networking/communication/communication.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-communication-and-networking', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-communication-and-networking/fundamentals-of-comminication-and-networking.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-data-representation', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-data-representation/fundamentals-of-data-representation.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-data-representation/number-bases', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-data-representation/number-bases/number-bases.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-data-representation/units-of-information', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-data-representation/units-of-information/units-of-information.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-data-representation/information-coding-systems', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-data-representation/information-coding-systems/information-coding-systems.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-data-representation/number-systems', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-data-representation/number-systems/number-systems.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-data-representation/representing-images-sounds-and-other-data', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-data-representation/representing-images-sounds-and-other-data/representing-images-sounds-and-other-data.html'
})
.when('/computer-science/aqa/unit-4/systematic-approach-to-problem-solving/aspects-of-software-development', {
templateUrl : 'paths/computer-science/aqa/unit-4/systematic-approach-to-problem-solving/aspects-of-software-development/aspects-of-software-development.html'
})
.when('/computer-science/aqa/unit-4/systematic-approach-to-problem-solving', {
templateUrl : 'paths/computer-science/aqa/unit-4/systematic-approach-to-problem-solving/systematic-approach-to-problem-solving.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-functional-programming/lists-in-functional-programming', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-functional-programming/lists-in-functional-programming/lists-in-functional-programming.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-functional-programming/writing-programming-functions', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-functional-programming/writing-programming-functions/writing-programming-functions.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-functional-programming/functional-programming-paradigm', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-functional-programming/functional-programming-paradigm/functional-programming-paradigm.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-functional-programming', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-functional-programming/fundamentals-of-functional-programming.html'
})
.when('/computer-science/aqa/unit-4/big-data', {
templateUrl : 'paths/computer-science/aqa/unit-4/big-data/big-data.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-databases/client-server-databases', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-databases/client-server-databases/client-server-databases.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-databases/sql', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-databases/sql/sql.html'
})
.when('/computer-science/aqa/unit-1/systematic-approach-to-problem-solving/aspects-of-software-development', {
templateUrl : 'paths/computer-science/aqa/unit-1/systematic-approach-to-problem-solving/aspects-of-software-development/aspects-of-software-development.html'
})
.when('/computer-science/aqa/unit-1/systematic-approach-to-problem-solving', {
templateUrl : 'paths/computer-science/aqa/unit-1/systematic-approach-to-problem-solving/systematic-approach-to-problem-solving.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-databases/database-design-and-normalization-techniques', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-databases/database-design-and-normalization-techniques/database-design-and-normalization-techniques.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-databases/relational-databases', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-databases/relational-databases/relational-databases.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-databases', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-databases/fundamentals-of-databases.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-databases/conceptual-data-models-and-entity-relationship-modelling', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-databases/conceptual-data-models-and-entity-relationship-modelling/conceptual-data-models-and-entity-relationship-modelling.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-communication-and-networking/the-transmission-control-protocol-and-internet-protocol', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-communication-and-networking/the-transmission-control-protocol-and-internet-protocol/the-transmission-control-protocol-and-internet-protocol.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-communication-and-networking/the-internet', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-communication-and-networking/the-internet/the-internet.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-communication-and-networking/networking', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-communication-and-networking/networking/networking.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-communication-and-networking/communication', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-communication-and-networking/communication/communication.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-communication-and-networking', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-communication-and-networking/fundamentals-of-comminication-and-networking.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-systems/logic-gates', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-systems/logic-gates/logic-gates.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-systems/boolean-algebra', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-systems/boolean-algebra/boolean-algebra.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-systems/types-of-translator', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-systems/types-of-translator/types-of-translator.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-systems/classification-of-programming-languages', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-systems/classification-of-programming-languages/classification-of-programming-languages.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-systems/hardware-and-software', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-systems/hardware-and-software/hardware-and-software.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-systems', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-systems/fundamentals-of-computer-systems.html'
})
.when('/computer-science/aqa/unit-4/consequences-of-uses-of-computing/individual-social-ethical-legal-and-cultural-issues-and-oppertunities', {
templateUrl : 'paths/computer-science/aqa/unit-4/consequences-of-uses-of-computing/individual-social-ethical-legal-and-cultural-issues-and-oppertunities/individual-social-ethical-legal-and-cultural-issues-and-oppertunities.html'
})
.when('/computer-science/aqa/unit-4/consequences-of-uses-of-computing', {
templateUrl : 'paths/computer-science/aqa/unit-4/consequences-of-uses-of-computing/consequences-of-uses-of-computing.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-organisation-and-architecture/external-hardware-devices', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-organisation-and-architecture/external-hardware-devices/external-hardware-devices.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-organisation-and-architecture/structure-and-role-of-the-processor-and-its-components', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-organisation-and-architecture/structure-and-role-of-the-processor-and-its-components/structure-and-role-of-the-processor-and-its-components.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-organisation-and-architecture/internal-hardware-components-of-a-computer', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-organisation-and-architecture/internal-hardware-components-of-a-computer/internal-hardware-components-of-a-computer.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-organisation-and-architecture/the-stored-program-concept', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-organisation-and-architecture/the-stored-program-concept/the-stored-program-concept.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-computer-organisation-and-architecture', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-computer-organisation-and-architecture/fundamentals-of-computer-organisation-and-architecture.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-data-representation/representing-images-sounds-and-other-data', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-data-representation/representing-images-sounds-and-other-data/representing-images-sounds-and-other-data.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-data-representation/units-of-information', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-data-representation/units-of-information/units-of-information.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-data-representation/number-bases', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-data-representation/number-bases/number-bases.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-data-representation/information-coding-systems', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-data-representation/information-coding-systems/information-coding-systems.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-data-representation/binary-number-systems', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-data-representation/binary-number-systems/binary-number-systems.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-data-representation/number-systems', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-data-representation/number-systems/number-systems.html'
})
.when('/computer-science/aqa/unit-4/fundamentals-of-data-representation', {
templateUrl : 'paths/computer-science/aqa/unit-4/fundamentals-of-data-representation/fundamentals-of-data-representation.html'
})
.when('/computer-science/aqa/unit-3/theory-of-computation/context-free-languages', {
templateUrl : 'paths/computer-science/aqa/unit-3/theory-of-computation/context-free-languages/context-free-languages.html'
})
.when('/computer-science/aqa/unit-3/theory-of-computation/a-model-of-computation', {
templateUrl : 'paths/computer-science/aqa/unit-3/theory-of-computation/a-model-of-computation/a-model-of-computation.html'
})
.when('/computer-science/aqa/unit-3/theory-of-computation/classification-of-algorithms', {
templateUrl : 'paths/computer-science/aqa/unit-3/theory-of-computation/classification-of-algorithms/classification-of-algorithms.html'
})
.when('/computer-science/aqa/unit-3/theory-of-computation/abstraction-and-automation', {
templateUrl : 'paths/computer-science/aqa/unit-3/theory-of-computation/abstraction-and-automation/abstraction-and-automation.html'
})
.when('/computer-science/aqa/unit-3/theory-of-computation/regular-languages', {
templateUrl : 'paths/computer-science/aqa/unit-3/theory-of-computation/regular-languages/regular-languages.html'
})
.when('/computer-science/aqa/unit-3/theory-of-computation', {
templateUrl : 'paths/computer-science/aqa/unit-3/theory-of-computation/theory-of-computation.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-algorithms/reverse-polish', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-algorithms/reverse-polish/reverse-polish.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-algorithms/sorting-algorithms', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-algorithms/sorting-algorithms/sorting-algorithms.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-algorithms/searching-algorithms', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-algorithms/searching-algorithms/searching-algorithms.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-algorithms/tree-traversal', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-algorithms/tree-traversal/tree-traversal.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-algorithms/graph-traversal', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-algorithms/graph-traversal/graph-traversal.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-algorithms', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-algorithms/fundamentals-of-algorithms.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-data-structures/vectors', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-data-structures/vectors/vectors.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-data-structures/hash-tables', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-data-structures/hash-tables/hash-tables.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-data-structures/dictionaries', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-data-structures/dictionaries/dictionaries.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-data-structures/trees', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-data-structures/trees/trees.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-data-structures/graphs', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-data-structures/graphs/graphs.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-data-structures/stacks', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-data-structures/stacks/stacks.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-data-structures/queues', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-data-structures/queues/queues.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-data-structures', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-data-structures/fundamentals-of-data-structures.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-programming/programming-paradigms', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-programming/programming-paradigms/programming-paradigms.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-programming/programming', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-programming/programming/programming.html'
})
.when('/computer-science/aqa/unit-3/fundamentals-of-programming', {
templateUrl : 'paths/computer-science/aqa/unit-3/fundamentals-of-programming/fundamentals-of-programming.html'
})
.when('/computer-science/aqa/unit-2/consequences-of-uses-of-computing/individual-social-ethical-legal-and-cultural-issues-and-oppertunities', {
templateUrl : 'paths/computer-science/aqa/unit-2/consequences-of-uses-of-computing/individual-social-ethical-legal-and-cultural-issues-and-oppertunities/individual-social-ethical-legal-and-cultural-issues-and-oppertunities.html'
})
.when('/computer-science/aqa/unit-2/consequences-of-uses-of-computing', {
templateUrl : 'paths/computer-science/aqa/unit-2/consequences-of-uses-of-computing/consequences-of-uses-of-computing.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-organisation-and-architecture/external-hardware-devices', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-organisation-and-architecture/external-hardware-devices/external-hardware-devices.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-organisation-and-architecture/internal-hardware-components-of-a-computer', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-organisation-and-architecture/internal-hardware-components-of-a-computer/internal-hardware-components-of-a-computer.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-organisation-and-architecture/the-stored-program-concept', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-organisation-and-architecture/the-stored-program-concept/the-stored-program-concept.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-organisation-and-architecture/structure-and-role-of-the-processor-and-its-components', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-organisation-and-architecture/structure-and-role-of-the-processor-and-its-components/structure-and-role-of-the-processor-and-its-components.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-organisation-and-architecture', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-organisation-and-architecture/fundamentals-of-computer-organisation-and-architecture.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-systems/boolean-algebra', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-systems/boolean-algebra/boolean-algebra.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-systems/classification-of-programming-languages', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-systems/classification-of-programming-languages/classification-of-programming-languages.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-systems/hardware-and-software', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-systems/hardware-and-software/hardware-and-software.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-systems/types-of-program-translator', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-systems/types-of-program-translator/types-of-program-translator.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-systems/logic-gates', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-systems/logic-gates/logic-gates.html'
})
.when('/computer-science/aqa/unit-2/fundamentals-of-computer-systems', {
templateUrl : 'paths/computer-science/aqa/unit-2/fundamentals-of-computer-systems/fundamentals-of-computer-systems.html'
})
.when('/computer-science/aqa/unit-1/theory-of-computation/abstraction-and-automation', {
templateUrl : 'paths/computer-science/aqa/unit-1/theory-of-computation/abstraction-and-automation/abstraction-and-automation.html'
})
.when('/computer-science/aqa/unit-1/theory-of-computation/finite-state-machines', {
templateUrl : 'paths/computer-science/aqa/unit-1/theory-of-computation/finite-state-machines/finite-state-machines.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-data-structures/data-structures-and-abstract-data-types', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-data-structures/data-structures-and-abstract-data-types/data-structures-and-abstract-data-types.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-programming/programming', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-programming/programming/programming.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-programming/procedural-oriented-programming', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-programming/procedural-oriented-programming/procedural-oriented-programming.html'
})
.when('/computer-science/aqa/unit-1/theory-of-computation', {
templateUrl : 'paths/computer-science/aqa/unit-1/theory-of-computation/theory-of-computation.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-data-structures', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-data-structures/fundamentals-of-data-structures.html'
})
.when('/computer-science/aqa/unit-1/fundamentals-of-programming', {
templateUrl : 'paths/computer-science/aqa/unit-1/fundamentals-of-programming/fundamentals-of-programming.html'
})
.when('/computer-science/ocr', {
templateUrl : 'paths/computer-science/ocr/ocr.html'
})
.when('/computer-science/aqa/unit-4', {
templateUrl : 'paths/computer-science/aqa/unit-4/unit-4.html'
})
.when('/computer-science/aqa/unit-3', {
templateUrl : 'paths/computer-science/aqa/unit-3/unit-3.html'
})
.when('/computer-science/aqa/unit-2', {
templateUrl : 'paths/computer-science/aqa/unit-2/unit-2.html'
})
.when('/computer-science/aqa/unit-1', {
templateUrl : 'paths/computer-science/aqa/unit-1/unit-1.html'
})
.when('/computer-science/pearsons', {
templateUrl : 'paths/computer-science/pearsons/pearsons.html'
})
.when('/computer-science/aqa', {
templateUrl : 'paths/computer-science/aqa/aqa.html'
})
.when('/computer-science', {
templateUrl : 'paths/computer-science/computer-science.html'
})
.when("/", {
    templateUrl : "paths/home/home.html"
})
.otherwise({
    redirectTo: "/"
});

//
}).config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]).controller('appCtrl', function(
    $scope, 
    $location, 
    $rootScope, 
    titleCaserService,
    uiService) {
    
    $rootScope.direct = function(url, event, $route) {
        angular.element(event.target).parent().parent().parent().parent()[0].scrollTop = 0;
        $location.url(url);
    };
    
    uiService.appLoaded().then(function() {
        $scope.title = 'ALVLS Revision' + ($location.url() === '/' ? ' ' : ' | ') + titleCaserService.titleCaser($location.url().split('/')[$location.url().split('/').length-1].replaceAll('-', ' '));
        $scope.description = description;
        window.prerenderReady = true;
    });
    
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };
            
    $scope.$on('$locationChangeSuccess', () => {
        $scope.title = 'ALVLS Revision' + ($location.url() === '/' ? ' ' : ' | ') + titleCaserService.titleCaser($location.url().split('/')[$location.url().split('/').length-1].replaceAll('-', ' '));
        $scope.description = description;
    });
    
    const description = `Learn your A-Level subjects, and answer questions on every topic. A-Level revision has never been easier!`;
    
}).service('uiService', function($http, $q, $timeout) {
        
        this.appLoaded = () => {
        const defer = $q.defer();
        const checkRenders = function() {
            if ($http.pendingRequests.length > 0) {
                $timeout(checkRenders);
            } else {
                defer.resolve(true);
            }
        };
        $timeout(checkRenders);
        return defer.promise;
        
    };
});
