describe('navbar', function() {
    beforeEach(module('RevisionApp'));
        
    var controller, scope;
    beforeEach(inject(function($controller, $rootScope, _$window_, _$timeout_, $compile) {
        
        //built in services
        $window = _$window_;
        $timeout = _$timeout_;
        compile = $compile;
        scope = $rootScope.$new();
        controller = $controller('navbarCtrl', {
            $scope: scope
        });
    }));
        
    describe('Ctrl', function() {

        //test for defined
        it('should be defined', function() {
            expect(controller).toBeDefined();
        });
        
        //triggered when the window is resized
        describe('resizing', function () {
            it('should reset the set search to false', function() {
               scope.search = true;
               angular.element($window).triggerHandler('resize');
                expect(scope.search).toBe(false);
            });
        })
    
        
        //function for selecting the search button
        describe('searchSelect', function() {
            it('should set focusLarge to true if desktop and nothing in the input field', function() {
                scope.focusSmall = false;
                scope.focusLarge = false;
                $window.innerWidth = 600;
                scope.searchVal = '';
                scope.searchSelect();
                expect(scope.focusLarge).toBe(true);
                expect(scope.focusSmall).toBe(false);
            });
            
            it('should set focusSmall to true if mobile and nothing in the input field', function() {
                scope.focusSmall = false;
                scope.focusLarge = false;
                $window.innerWidth = 599;
                scope.searchVal = '';
                scope.searchSelect();
                $timeout.flush();
                expect(scope.focusSmall).toBe(true);
                expect(scope.focusLarge).toBe(false);
            });
            
            it('should set search to true if mobile and nothing in the input field', function() {
                scope.search = false;
                $window.innerWidth = 599;
                scope.searchVal = '';
                scope.searchSelect();
                expect(scope.search).toBe(true);
            });
        });
        
        describe('backSelect', function() {
            it('should set search to false', function() {
                scope.search = true;
                scope.backSelect();
                expect(scope.search).toBe(false);
            });
        });
        
    });
});