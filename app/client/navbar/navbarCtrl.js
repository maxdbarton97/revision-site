angular.module('RevisionApp')
.controller('navbarCtrl', function($scope, $location, $window, $timeout, $mdSidenav, $mdComponentRegistry) {
    
    $scope.search = false;
    $scope.menu = false;
    $scope.focusLarge = false;
    $scope.focusSmall = false;
    
    $scope.homeSelect = function() {
        $location.url('/');
    };
    
    $scope.searchVal = '';
    angular.element($window).on('resize', function() {
        $scope.search = false;
    });
    
    $mdComponentRegistry.when('left').then(function() {
        $scope.$watch(function() {
            return $mdSidenav('left').isOpen();
        }, function(val) {
            $scope.menu = val;
            if (val) $scope.icon = 'close';
            else $scope.icon = 'menu';
        });
    });
    
    $scope.searchSelect = function() {
        $mdSidenav('left').close();
        if ($window.innerWidth >= 600 && $scope.searchVal.length === 0) {
            $scope.focusLarge = true;
        } else if ($scope.searchVal.length === 0) {
            $scope.search = true;
            $scope.icon = 'keyboard_backspace',
            $timeout(function() {
                $scope.focusSmall = true;
            });
        }
        else {
            //search the website
        }
    };
    
    $scope.iconSelect = {
        close: function() {$mdSidenav('left').close()},
        menu: function() {$mdSidenav('left').open()},
        keyboard_backspace: function() {$scope.search = false}
    };
    
})