angular.module('RevisionApp')
.controller('viewCtrl', function($scope, $mdSidenav, $mdComponentRegistry, $location) {

    $scope.$watch(function() {
        return $location.url();
    }, function(val) {
        $scope.home = false;
        if (val === '/'){
            $scope.home = true;
        }
    });
    
})