describe('titlecase', function() {
    beforeEach(module('RevisionApp'));
      
    var element, scope, compile;
    beforeEach(function() { 
        inject(function($compile, $rootScope, _$timeout_){
            scope = $rootScope.$new();
            compile = $compile;
        });
        
        element = angular.element('<div title-case></div>');
    });
        
    describe('Title Case Directive', function() {
        
        it('should capitalize the first and last word', function() {
            //using two prepositions which shouldn't be capitalized unless first or last word
            element.html('to by');
            element = compile(element)(scope);
            scope.$digest();
            expect(element.html()).toBe('To By');
        });
        
        it('should not capitalize articles', function() {
            element.html('around the corner');
            element = compile(element)(scope);
            scope.$digest();
            expect(element.html()).toBe('Around the Corner');
        });
        
        it('should not capitalize coordinating conjuctions', function() {
            element.html('cheese for me and you');
            element = compile(element)(scope);
            scope.$digest();
            expect(element.html()).toBe('Cheese for Me and You');
        });
        
        it('should not capitalize prepositions under 5 letters long', function() {
            element.html('a pace by the river');
            element = compile(element)(scope);
            scope.$digest();
            expect(element.html()).toBe('A pace by the River');
        });
        
        it('should capitalize prepositions 5 or more letters long', function() {
            element.html('Arsenal versus chelsea');
            element = compile(element)(scope);
            scope.$digest();
            expect(element.html()).toBe('Arsenal Versus Chelsea');
        });
        
        it('should lowercase letters that are not the firt letter of words', function() {
            element.html('ALL CAPS');
            element = compile(element)(scope);
            scope.$digest();
            expect(element.html()).toBe('All Caps');
        });
        
        it('should look for expections that have manually been added', function() {
            element.html('aqa');
            element = compile(element)(scope);
            scope.$digest();
            expect(element.html()).toBe('AQA');
        });
        
        
    });
    
});