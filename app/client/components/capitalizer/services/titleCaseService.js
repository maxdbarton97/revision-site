/*
 * A service which allows the developer to use the titleCaser method, where a
 * string is passed, and then returns the correctly title cased string back.
 */

angular.module('RevisionApp')
.service('titleCaserService', function() {
    
    //=========================================================================
    // variables
    //=========================================================================
    
    // do NOT title case these strings, where uppercase, lowercase and some 
    // symbols should remain preserved.
    var exceptions = [
        'AQA',
        'OCR',
        'Graph-Traversal',
        'Tree-Traversal',
        'Context-Free',
        'SQL',
        'ALVLS'
    ];
            
    // words that should not be capitalised
    var articles = ['a', 'an', 'the'];
    var coordConjs = ['for', 'and', 'nor', 'but', 'or', 'yet','so'];
    var prepositions = ['aboard', 'about', 'above', 'absent', 'across', 'cross', 'after',
        'against', 'along', "'gainst", 'gainst', 'again', 'gain', 'along', 'alongside',
        'amid', 'amidst', 'mid', 'midst', 'among', 'amongst', "'mong", 'mong', "'mongst",
        'apropos', 'apud', 'around', 'as', 'astride', 'at', 'atop', 'ontop', 'bar', 'before',
        'afore', 'tofore', 'behind', 'ahind', 'below', 'ablow', 'allow', 'beneath', "'neath",
        'neath', 'beside', 'besides', 'between', 'atween', 'beyond', 'ayond', 'but', 'by',
        'chez', 'circa', 'c.', 'ca.', 'come', 'dehors', 'despite', 'spite', 'down', 'during',
        'except', 'for', 'from', 'in', 'inside', 'into', 'less', 'like', 'minus', 'near',
        'nearer', 'nearest', 'anear', 'notwithstanding', 'of', "o'", 'pace', 'past', 'per',
        'post', 'pre', 'pro', 'qua', 're', 'snas', 'save', 'sauf', 'short', 'since',
        'sithence', 'than' ,'through', 'thru', 'throughout', 'thruout', 'to', 'toward',
        'towards', 'under', 'underneath', 'unlike', 'until', "'till", 'til', 'till', 'up',
        'upon', "'pon", 'pon', 'upside', 'versus', 'vs', 'v.', 'via', 'vice', 'vis-à-vis',
        'with', 'w/', 'c', 'within', 'w/i', 'without', 'w/o', 'worth'
    ];

    var word;
    
    //=========================================================================
    // service methods
    //=========================================================================
    
    this.titleCaser = function(str) {
        let res = [];
        var words = str.split(' ');
        words.forEach(function(value, index) {
            word = value.toLowerCase();
            //check if in exception
            exceptions.forEach(function(value2, index2) {
                if (word === exceptions[index2].toLowerCase()) {
                    res.push(exceptions[index2]);
                }
            });
            
            if ((articles.indexOf(word) === -1 && coordConjs.indexOf(word) === -1 && prepositions.indexOf(word) === -1) ||
            (prepositions.indexOf(word) > -1 && value.length > 4) ||
            index === 0 ||
            index === words.length-1) {
                word = word.slice(0, 1).toUpperCase() + word.slice(1, value.length);
                if (res.length !== index+1) res.push(word);
            } else if (res.length !== index+1) res.push(word);
            
        });
        
        res = res.join(' ');
        return res;
    };
    
});