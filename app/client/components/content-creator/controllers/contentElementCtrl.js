/*
 * The content element controller is used in every content element to manage scope
 * logic. For example, the drag variable is enabled and disabled depending on if
 * the element has been dropped on or now, to allow the database to write before
 * trying to reorder again.
 */

angular.module('RevisionApp')
.controller('contentElementCtrl', function(
    $element,
    $rootScope,
    $scope,
    $timeout,
    contentElementCountService,
    fileWriteService,
) {
    
    //=========================================================================
    // scope variables and local variables
    //=========================================================================
    
    $rootScope.drag = true;
    var targetElNumber = Number($element.attr('edit-content-element'));
    
    //=========================================================================
    // scope functions
    //=========================================================================
    
    // when an element is dropped onto this element, determine if it's being 
    // dragged backwards or forwards, and reassign the content element id's accordingly.
    $scope.drop = function(event) {
        $rootScope.drag = false;
        $scope.$digest();
        var moveElNumber = Number(event.element.attr('edit-content-element'));
        
        //backwards?
        if (moveElNumber > targetElNumber) {
            for (var x = 0; x < contentElementCountService.get(); x++) {
                $rootScope.$broadcast('reorderBack'+x.toString(), {
                    moveElNumber: moveElNumber,
                    targetElNumber: targetElNumber
                });
            }
            
        //forwards..
        } else if (moveElNumber < targetElNumber - 1) {
            for (var x = 0; x < contentElementCountService.get(); x++) {
                $rootScope.$broadcast('reorderForward'+x.toString(), {
                    moveElNumber: moveElNumber,
                    targetElNumber: targetElNumber
                });
            }
        }
        
        fileWriteService.reorder($element.parent().children());
        //after 2 seconds (to allow database to write in good time)
        $timeout(function() {
            $rootScope.drag = true;
            $scope.$digest();
        }, 2000);
        
        //reassign the ordering functions so that they use their new index number
        $rootScope.$broadcast('reassignOrderingFunctions');

    };
});