angular.module('RevisionApp')
.directive('editRemove', function($http, $rootScope, activeInputService, fileWriteService) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            
            element.on('click', function() { 
                
                //find content-element number
                var elCount = element.parent().attr('edit-content-element');
                
                //broadcast to controller to write to file
                fileWriteService.delete(elCount);
                
                //remove image from database
                if (element.parent().attr('type') === 'image') {
                    //get uuid
                    var imageUuid = element.parent().attr('uuid');
                    var base = element.parent().attr('base');
                    var obj = {};
                    obj[imageUuid] = base;
                    //delete from databse
                    $http.patch('/remove-image', obj).then(function(data) {
                        //remove the edit element
                        element.parent().remove();
                    });
                } else {
                    //remove the edit element
                    element.parent().remove();
                }
            });
        }
    };
});