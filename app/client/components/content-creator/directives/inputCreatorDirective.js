angular.module('RevisionApp')
.directive('addContent', function($compile, $document, activeInputService, templateMapService) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            
            var insertion = angular.element($document[0].getElementById('insertion'));
            
            var temp;
            element.on('click', function() {
                if (activeInputService.get() === false) {
                    activeInputService.set(true);
                    var el = angular.element(templateMapService.createInputs[element.attr('type')]);
                    insertion.append(el);
                    $compile(el)(scope);
                }
            });
            
        }
    };
});