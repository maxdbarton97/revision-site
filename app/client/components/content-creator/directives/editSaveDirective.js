angular.module('RevisionApp')
.directive('editSave', function($rootScope, templateMapService, $compile, fileWriteService) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            
            //variables
            var compile = true;
            
            //save original element for comparisson to changes string
            var originalElement = angular.copy(element.parent());
            
            //function for replacing all instances of a substring with a new one
            String.prototype.replaceAll = function(search, replacement) {
                var target = this;
                return target.split(search).join(replacement);
            };
            
            element.on('click', function() {
                //get final element template
                var el = angular.element(templateMapService.finals[element.parent().attr('type')]);
                //if question
                if (element.parent().attr('type') === 'question') {
                    //get new question text
                    var newQuestion = angular.element(element.parent().children().eq(0).find('textarea')[0]).val();
                    //get new answer 1 text
                    var newA1 = angular.element(element.parent().children().eq(0).find('textarea')[1]).val();
                    //get new answer 2 text
                    var newA2 = angular.element(element.parent().children().eq(0).find('textarea')[2]).val();
                    //get new answer 3 text
                    var newA3 = angular.element(element.parent().children().eq(0).find('textarea')[3]).val();
                    //get new answer 4 text
                    var newA4 = angular.element(element.parent().children().eq(0).find('textarea')[4]).val();
                    //get original question text
                    var oldQuestion = angular.element(originalElement.children().eq(0).find('textarea')[0]).val();
                    //get original answer 1 text
                    var oldA1 = angular.element(originalElement.children().eq(0).find('textarea')[1]).val();
                    //get original answer 2 text
                    var oldA2 = angular.element(originalElement.children().eq(0).find('textarea')[2]).val();
                    //get original answer 3 text
                    var oldA3 = angular.element(originalElement.children().eq(0).find('textarea')[3]).val();
                    //get original answer 4 text
                    var oldA4 = angular.element(originalElement.children().eq(0).find('textarea')[4]).val();
                    //get new checkbox
                    for (var x=0; x < element.parent().children().eq(1).find('md-checkbox').length; x++) {
                        if (angular.element(element.parent().children().eq(1).find('md-checkbox')[x]).hasClass('md-checked')) {
                            var newCorrect = x; 
                        }
                    }
                    //get original checkbox
                    for (var x=0; x < originalElement.children().eq(1).find('md-checkbox').length; x++) {
                        if (angular.element(originalElement.children().eq(1).find('md-checkbox')[x]).hasClass('md-checked')) {
                            var oldCorrect = x; 
                        }
                    }
                    //if not empty and if the new strings aren't equal to the old
                    if (
                    newQuestion.length > 0 &&
                    newA1.length > 0 &&
                    newA2.length > 0 &&
                    newA3.length > 0 &&
                    newA4.length > 0 &&
                    newCorrect !== undefined &&
                    (newQuestion !== oldQuestion || 
                    newA1 !== oldA1 ||
                    newA2 !== oldA2 ||
                    newA3 !== oldA3 ||
                    newA4 !== oldA4 ||
                    oldCorrect !== newCorrect)) {
                        //append question to the question element
                        el.children().eq(0).html(newQuestion);
                        //append answer 1 to the answer 1 element
                        el.children().eq(1).children().eq(0).html(newA1);
                        //append answer 2 to the answer 1 element
                        el.children().eq(2).children().eq(0).html(newA2);
                        //append answer 3 to the answer 1 element
                        el.children().eq(3).children().eq(0).html(newA3);
                        //append answer 4 to the answer 1 element
                        el.children().eq(4).children().eq(0).html(newA4);
                        //append new correct answer
                        el.children().eq(newCorrect+1).attr('correct', true);
                        //get content element number
                        var elCount = element.parent().attr('edit-content-element');
                        //set the final elements content number
                        el.attr('content-element', elCount);
                        //create a new element
                        var elCon = angular.element('<div></div>');
                        //append the final element to it
                        elCon.append(el);
                        //now use the html() method to get the inner HTML as text, instead of as an object
                        var newElement = `    ${elCon.html()}`;
                        //use file writing service to append final element in place of old
                        fileWriteService.edit(newElement, elCount);
                        //get an edit content element
                        el = angular.element(templateMapService.edits[element.parent().attr('type')]);
                        //append question to the question element
                        el.children().eq(0).html(newQuestion);
                        //append answer 1 to the answer 1 element
                        el.children().eq(1).children().eq(0).html(newA1);
                        //append answer 2 to the answer 1 element
                        el.children().eq(2).children().eq(0).html(newA2);
                        //append answer 3 to the answer 1 element
                        el.children().eq(3).children().eq(0).html(newA3);
                        //append answer 4 to the answer 1 element
                        el.children().eq(4).children().eq(0).html(newA4);
                        //append new correct answer
                        el.children().eq(newCorrect+1).attr('correct', true);
                    } else {
                        compile = false;
                    }
                //definition
                } else if (element.parent().attr('type') === 'definition') {
                    //get new definable text
                    var newDefinable = angular.element(element.parent().find('textarea')[0]).val();
                    //get new definition text
                    var newDefinition = angular.element(element.parent().find('textarea')[1]).val();
                    //get original definable text
                    var oldDefinable = angular.element(originalElement.find('textarea')[0]).val();
                    //get original definition text
                    var oldDefinition = angular.element(originalElement.find('textarea')[1]).val();
                    //if not empty and if the new strings aren't equal to the old
                    if (
                    newDefinable.length > 0 &&
                    newDefinition.length > 0 &&
                    (newDefinable !== oldDefinable ||
                    newDefinition !== oldDefinition)) {
                        //append the definable to the definable element
                        el.children().eq(0).html(newDefinable);
                        //append the definition to the definition element
                        el.children().eq(1).html(`: ${newDefinition}`);
                        //get content element number
                        var elCount = element.parent().attr('edit-content-element');
                        //set the final elements content number
                        el.attr('content-element', elCount);
                        //create a new element
                        var elCon = angular.element('<div></div>');
                        //append the final element to it
                        elCon.append(el);
                        //now use the html() method to get the inner HTML as text, instead of as an object
                        var newElement = `    ${elCon.html()}`;
                        //use file writing service to append final element in place of old
                        fileWriteService.edit(newElement, elCount);
                        //get an edit content element
                        el = angular.element(templateMapService.edits[element.parent().attr('type')]);
                        //append the definable to the definable element
                        el.children().eq(0).html(newDefinable);
                        //append the definition to the definition element
                        el.children().eq(1).html(`: ${newDefinition}`);
                    } else {
                        compile = false;
                    }
                    
                //code
                } else if (element.parent().attr('type') === 'code') {
                    var newInput = angular.element(element.parent().find('textarea')[0]).val();
                    var newOutput = angular.element(element.parent().find('textarea')[1]).val();
                    var input = angular.element(originalElement.find('textarea')[0]).val();
                    var output = angular.element(originalElement.find('textarea')[1]).val();
                    if (
                    newInput.length > 0 &&
                    newOutput.length > 0 &&
                    (newInput !== input ||
                    newOutput !== output)) {
                        newInput = newInput.replaceAll('\n', '<br>');
                        newOutput = newOutput.replaceAll('\n', '<br>');
                        el.children().eq(0).children().eq(1).html(newInput);
                        el.children().eq(1).children().eq(1).html(newOutput);
                        var elCount = element.parent().attr('edit-content-element');
                        el.attr('content-element', elCount);
                        var elCon = angular.element('<div></div>');
                        //append the final element to it
                        elCon.append(el);
                        //now use the html() method to get the inner HTML as text,
                        //instead of as an object
                        var newElement = `    ${elCon.html()}`;
                        //use file writing service to append final element in place of old
                        fileWriteService.edit(newElement, elCount);
                        //get an edit content element
                        el = angular.element(templateMapService.edits[element.parent().attr('type')]);
                        el.children().eq(0).children().eq(1).html(newInput);
                        el.children().eq(1).children().eq(1).html(newOutput);
                    } else {
                        compile = false;
                    }
                    
                //else if single input content type
                } else {
                    //new string
                    var newString = element.parent().find('textarea').val();
                    //if not empty and if the new string is not the same as old string
                    var originalString = originalElement.find('textarea').val();
                    if (newString.length > 0 && newString !== originalString) {
                        //append the text to it
                        el.append(newString);
                        //get the edit content element number
                        var elCount = element.parent().attr('edit-content-element');
                        //set the final elements content number
                        el.attr('content-element', elCount);
                        //create a new element
                        var elCon = angular.element('<div></div>');
                        //append the final element to it
                        elCon.append(el);
                        //now use the html() method to get the inner HTML as text, instead of as an object
                        var newElement = `    ${elCon.html()}`;
                        //use file writing service to append final element in place of old
                        fileWriteService.edit(newElement, elCount);
                        //get an edit content element
                        el = angular.element(templateMapService.edits[element.parent().attr('type')]);
                        //append new text
                        el.html(newString);
                    } else {
                        compile = false;
                    }
                }
                
                if (compile) { 
                    //set edit content element number
                    el.attr('edit-content-element', elCount);
                    //compile it so that changes can be made and directives are applied
                    $compile(el)(scope);
                    //replace the new edit content element with this old one
                    element.parent().replaceWith(el);
                } else {
                    //set true for next attempt
                    compile = true;
                }
    
            });
        }
    };
});