/*
 * A directive that is applied to the image creation element, which extends its
 * functionality by providing a scope variable which converts the uploaded 
 * image into a base 64 string, and can handle upperbounds for the file size.
 */

angular.module('RevisionApp')
.directive('createImage', function() {
    return { link: function(scope, element, attrs) {
            
        //=====================================================================
        // scope and local variables
        //=====================================================================
        
        scope.image = undefined;
        const fileReader = new FileReader();
        
        //=====================================================================
        // init
        //=====================================================================
        
        element.removeAttr('base-64-data');
        
        //=====================================================================
        // functions
        //=====================================================================
        
        // the image scope variable is binded to the element that handles the 
        // image file, so watch it and when theres an image, run the
        // imageUpload function
        scope.$watch('image', function(val) {
            if (val) scope.imageUpload(val);
        });
        
        scope.imageUpload = function(image) {
            if (image.size < 1000000) {
                element.html('Uploading...');
                var fileReader = new FileReader();
                fileReader.readAsDataURL(image);
                var base64Data;
                fileReader.onload = function (e) {
                    var dataUrl = e.target.result;
                    base64Data = dataUrl.substr(dataUrl.indexOf('base64,') + 'base64,'.length);
                    //set new data
                    element.html(image.name);
                    element.attr('base-64-data', base64Data);
                };  
            } else {
                 element.html('Image must be under 1MB in size.');
                 scope.image = undefined;
                 element.removeAttr('base-64-data');
            }
        };
            
    }};
});