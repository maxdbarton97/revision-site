angular.module('RevisionApp')
.directive('editCancel', function($compile, activeInputService, templateMapService) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            
            var el;
            String.prototype.replaceAll = function(search, replacement) {
                var target = this;
                return target.split(search).join(replacement);
            };
            
            element.on('click', function() {
                el = angular.element(element.parent().attr('prev'));
                if (el.attr('type') === 'question') {
                    for (var x=0; x < el.children().length; x++) {
                        //remove transclude
                        el.children().eq(x).removeAttr('ng-transclude');
                    }
                }
                $compile(el)(scope);
                activeInputService.set(false);
                element.parent().replaceWith(el);

            });
            
        }
    };
});