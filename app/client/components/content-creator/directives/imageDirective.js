angular.module('RevisionApp')
.directive('image', function($rootScope, $compile, $document, templateMapService, $http, loadingService, $window) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            
            var width;
            
            element.ready(function() {
                
                //if not created in current session
                if (!element.attr('src') && element.parent().children().eq((element.parent().children().length-1))[0]) {
                    //if in edit
                    if (element.parent().children().eq((element.parent().children().length-1))[0].nodeName === 'CONTENT') {
                        if (element.attr('edit')) {
                            loadingService.start(element);
                        }
                    //if final
                    } else {
                        loadingService.start(element);
                    }
                    //get image uuid
                    var imageUuid = element.attr('uuid');
                    //get string
                    $http.get('/get-image', {
                        params: {uuid: imageUuid}
                    }).then(function(data) {
                        //if in edit
                        if (element.parent().children().eq((element.parent().children().length-1))[0].nodeName === 'CONTENT') {
                            if (element.attr('edit')) {
                                loadingService.end(element);
                            }
                        //if final
                        } else {
                            loadingService.end(element);
                        }
                        //assing base64Data to src to display image
                        element.attr('src', `data:image/jpeg;base64,${data.data[imageUuid]}`);
                        //set width
                        width = element[0].offsetWidth;
                        checkSize();
                    });
                }
            });
            
            //apply correct class on resize
            function checkSize() {
                if (element.parent()[0].offsetWidth < width) {
                    //apply 100% width class
                    element.addClass('image-max-width');
                } else {
                    //remove 100% width
                    element.removeClass('image-max-width');
                }
            }
            
            //on resize, check the image size
            angular.element($window).on('resize', function() {
                checkSize();
            });

        }
    };
});