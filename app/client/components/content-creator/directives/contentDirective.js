/*
 * The content directive can be written on any page, and will traverse through the 
 * HTML and make each content element, with a content element attribute and relevent
 * ID number, to then convert them into editable element.
 * 
 * Initially, This would be done manually, however it made it easier as a developer 
 * to just include the content element on all pages and then implement a discrete way
 * to active it, in this case holding CTRL + Space for 5 seconds.
 */

angular.module('RevisionApp')
.directive('content', function(
    $compile,
    $document,
    $timeout,
    $window,
    templateMapService
) {
    return {
        restrict: 'E',
        templateUrl: '/app/client/components/content-creator/content.html',
        link: function(scope, element, attrs) {
            
            //=========================================================================
            // variables
            //=========================================================================
            
            var contentElements = angular.element(document.querySelectorAll("[content-element]"));
            
            let ctrl = false; 
            let space = false;
            let isTimer = false;
            let timer = false;
            scope.isEdit = false;
            
            //=========================================================================
            // listeners
            //=========================================================================
            
            angular.element($window).on('keydown', (event) => {
                if (!scope.isEdit) {
                    if (event.code == 'ControlLeft' 
                    || event.code == 'ControlRight') ctrl = true;
                    if (event.code == 'Space') space = true;
                    if (space && ctrl) {
                        // start timer
                        if (isTimer == false) {
                            isTimer = true;
                            timer = $timeout(() => {
                                scope.isEdit = true;
                                convertElements();
                            }, 5000);
                        } else {
                            if (event.code != 'ControlLeft'
                            && event.code != 'ControlRight' 
                            && event.code != 'Space') {
                                isTimer = false;
                                $timeout.cancel(timer);
                            }
                        }
                    }
                }
            });
            
            angular.element($window).on('keyup', (event) => {
                if (!scope.isEdit) {
                    if (isTimer == true) {
                        isTimer = false;
                        $timeout.cancel(timer);
                    }
                }
            });
            
            //=========================================================================
            // functions
            //=========================================================================
            
            // for each content element, iterate through, and replace them with editable
            // elements
            function convertElements() {
                const insertion = angular.element($document[0].getElementById('insertion'));
                angular.forEach(contentElements, function(value, index) {
                    angular.element(value).css('display', 'none');
                    var el = angular.element(templateMapService.edits[angular.element(value).attr('type')]);
                    //get and set content-element number
                    el.attr('edit-content-element', angular.element(value).attr('content-element'));
                    //if image, pass the uuid through for unique id
                    if (angular.element(value).attr('uuid')) el.attr('uuid', angular.element(value).attr('uuid'));
                    $compile(el)(scope);
                    el.html(angular.element(value).html());
                    insertion.append(el);
                });
            }

 
        }
    };
});