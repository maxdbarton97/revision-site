/*
 * Directive that is applied to the create button element that creates a 
 * listener on the element so when clicked, the element is created for the first 
 * time.
 */

angular.module('RevisionApp')
.directive('createSave', function(
    $compile,
    $document,
    $http,
    contentElementCountService,
    fileWriteService,
    templateMapService,
    uuid
) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            
            //=================================================================
            // variables 
            //=================================================================
            
            let elCon, elText, el, elInsert, elCount, insertion, textareas;
            
            //=================================================================
            // helper functions 
            //=================================================================
            
            String.prototype.replaceAll = function(search, replacement) {
                let target = this;
                return target.split(search).join(replacement);
            };
            
            //=================================================================
            // listeners 
            //=================================================================
            
            // when the element is saved for the first time, use the type
            // attribute of the element to determine what type of element
            // will be created, and then set the write information and write 
            // the html to the correct file using node
            element.on('click', function() {
                //image
                if (element.parent().attr('type') === 'image') {
                    if (scope.$eval(element.parent().children().eq(0).attr('ng-model'))) {
                        let imageUuid = uuid.new();
                        let imageBase64Data = element.parent().children().eq(0).attr('base-64-data');
                        //new image into base-map.js through node server
                        $http.post('/save-new-image', {
                            [imageUuid]: imageBase64Data,
                        }).then(function() {
                            
                            //get correct template to save
                            el = angular.element(templateMapService.finals[element.parent().attr('type')]);
                            elInsert = angular.element(templateMapService.edits[element.parent().attr('type')]);
        
                            elCount = contentElementCountService.get();
                            el.attr('content-element', elCount);
                            elInsert.attr('edit-content-element', elCount);
            
                            //set uuid as attr for final and edit
                            el.attr('uuid', imageUuid);
                            elInsert.attr('uuid', imageUuid);
                            elInsert.attr('src', `data:image/jpeg;base64,${imageBase64Data}`);
                            
                            //convert to text
                            elCon = angular.element('<div></div>');
                            elCon.append(angular.copy(el));
                            elText =`    ${elCon.html()}`;
                            
                            //broadcast data to controller to write to file
                            fileWriteService.create(elText);
                            
                            //create content version for viewing
                            insertion = angular.element($document[0].getElementById('insertion'));
                            
                            $compile(elInsert)(scope);
                            insertion.append(elInsert);
                            
                            //remove the input box
                            element.parent().remove();
                            
                        });
                    }
                    
                //question
                } else if (element.parent().attr('type') === 'question') {
                    //if question
                    //get textareas
                    textareas = element.parent().children().eq(0).find('textarea');
                    let question = angular.element(textareas[0]).val();
                    let a1 = angular.element(textareas[1]).val();
                    let a2 = angular.element(textareas[2]).val();
                    let a3 = angular.element(textareas[3]).val();
                    let a4 = angular.element(textareas[4]).val();
                    //get checkbox value
                    let checkboxes = element.parent().children().eq(1).find('md-checkbox');
                    //find correct answer
                    let correctAnswer;
                    for (let x=0; x < checkboxes.length; x++) {
                        if (angular.element(checkboxes[x]).hasClass('md-checked')) {
                            correctAnswer = x;
                            break
                        }
                    }
                    if (
                    question.length > 0 &&
                    a1.length > 0 &&
                    a2.length > 0 &&
                    a3.length > 0 && 
                    a4.length > 0 &&
                    correctAnswer !== undefined) {
                        
                        //get element templates
                        el = angular.element(templateMapService.finals[element.parent().attr('type')]);
                        elInsert = angular.element(templateMapService.edits[element.parent().attr('type')]);
                        
                        //elCount
                        elCount = contentElementCountService.get();
                        el.attr('content-element', elCount);
                        elInsert.attr('edit-content-element', elCount);
                        
                        //insert values into tempalates
                        //question
                        el.children().eq(0).html(question);
                        elInsert.children().eq(0).html(question);
                        //answers
                        el.children().eq(1).children().eq(0).html(a1.replaceAll('\n', '<br>'));
                        elInsert.children().eq(1).children().eq(0).html(a1.replaceAll('\n', '<br>'));
                        el.children().eq(2).children().eq(0).html(a2.replaceAll('\n', '<br>'));
                        elInsert.children().eq(2).children().eq(0).html(a2.replaceAll('\n', '<br>'));
                        el.children().eq(3).children().eq(0).html(a3.replaceAll('\n', '<br>'));
                        elInsert.children().eq(3).children().eq(0).html(a3.replaceAll('\n', '<br>'));
                        el.children().eq(4).children().eq(0).html(a4.replaceAll('\n', '<br>'));
                        elInsert.children().eq(4).children().eq(0).html(a4.replaceAll('\n', '<br>'));
                        //correct answer
                        el.children().eq(correctAnswer+1).attr('correct', true);
                        elInsert.children().eq(correctAnswer+1).attr('correct', true);
                        
                        //convert to text
                        elCon = angular.element('<div></div>');
                        elCon.append(angular.copy(el));
                        elText =`    ${elCon.html()}`;
                        
                        //broadcast data to controller to write to file
                        fileWriteService.create(elText);
    
                        //create content version for viewing
                        insertion = angular.element($document[0].getElementById('insertion'));
                        
                        $compile(elInsert)(scope);
                        insertion.append(elInsert);
                        
                        //remove the input box
                        element.parent().remove();
                    }
                    
                //if definition
                } else if (element.parent().attr('type') === 'definition') {
                    
                    //get textareas
                    textareas = element.parent().find('textarea');
                    let definable = angular.element(textareas[0]).val();
                    let  definition = angular.element(textareas[1]).val();
                    
                    if (
                    definable.length > 0 &&
                    definition.length > 0) {
                        
                    //get element templates
                    el = angular.element(templateMapService.finals[element.parent().attr('type')]);
                    elInsert = angular.element(templateMapService.edits[element.parent().attr('type')]);
                        
                    //elCount
                    elCount = contentElementCountService.get();
                    el.attr('content-element', elCount);
                    elInsert.attr('edit-content-element', elCount);
                        
                    //definable
                    el.children().eq(0).html(definable);
                    elInsert.children().eq(0).html(definable);
                    //definition
                    el.children().eq(1).html(`: ${definition}`);
                    elInsert.children().eq(1).html(`: ${definition}`);
                    
                    //convert to text
                    elCon = angular.element('<div></div>');
                    elCon.append(angular.copy(el));
                    elText =`    ${elCon.html()}`;
                        
                    //broadcast data to controller to write to file
                    fileWriteService.create(elText);
    
                    //create content version for viewing
                    insertion = angular.element($document[0].getElementById('insertion'));
                        
                    $compile(elInsert)(scope);
                    insertion.append(elInsert);

                    //remove the input box
                    element.parent().remove();
                    
                    }
                    
                } else if (element.parent().attr('type') === 'code') {
                    
                    //get textareas
                    textareas = element.parent().find('textarea');
                    let input = angular.element(textareas[0]).val();
                    let output = angular.element(textareas[1]).val();
                    input = input.replaceAll('\n', '<br>');
                    output = output.replaceAll('\n', '<br>');
                    
                    if (
                    input.length > 0 &&
                    output.length > 0) {
                        
                    //get element templates
                    el = angular.element(templateMapService.finals[element.parent().attr('type')]);
                    elInsert = angular.element(templateMapService.edits[element.parent().attr('type')]);
                        
                    //elCount
                    elCount = contentElementCountService.get();
                    el.attr('content-element', elCount);
                    elInsert.attr('edit-content-element', elCount);
                        
                    //input
                    el.children().eq(0).children().eq(1).html(input);
                    elInsert.children(0).eq(0).children().eq(1).html(input);
                    //output
                    el.children().eq(1).children().eq(1).html(output);
                    elInsert.children().eq(1).children().eq(1).html(output);
                    
                    //convert to text
                    elCon = angular.element('<div></div>');
                    elCon.append(angular.copy(el));
                    elText =`    ${elCon.html()}`;
                        
                    //broadcast data to controller to write to file
                    fileWriteService.create(elText);
    
                    //create content version for viewing
                    insertion = angular.element($document[0].getElementById('insertion'));
                        
                    $compile(elInsert)(scope);
                    insertion.append(elInsert);

                    //remove the input box
                    element.parent().remove();

                    }
                    
                //single input content
                } else {
                    if (element.parent().find('textarea').val().length > 0) {
                        //get correct template to save
                        el = angular.element(templateMapService.finals[element.parent().attr('type')]);
                        elInsert = angular.element(templateMapService.edits[element.parent().attr('type')]);
                        
                        //set correct count
                        //get number of elements here
                        elCount = contentElementCountService.get();
    
                        el.attr('content-element', elCount);
                        elInsert.attr('edit-content-element', elCount);
        
                        //set html to the input
                        let inputText = element.parent().find('textarea').val();
                        inputText = inputText.replaceAll('\n', '<br>');
                        el.html(inputText);
                        elInsert.html(inputText);
                                                
                        
                        //convert to text
                        elCon = angular.element('<div></div>');
                        elCon.append(angular.copy(el));
                        elText =`    ${elCon.html()}`;
                        
                        //broadcast data to controller to write to file
                        fileWriteService.create(elText);
    
                        //create content version for viewing
                        insertion = angular.element($document[0].getElementById('insertion'));
                        
                        $compile(elInsert)(scope);
                        insertion.append(elInsert);
                        
                        //remove the input box
                        element.parent().remove();
                    }
                }
                
            });
        }
    };
});