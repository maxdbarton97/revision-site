angular.module('RevisionApp')
.directive('lineBreak', function($compile, $document, activeInputService, templateMapService) {
    return {
        replace: true,
        template: '<div class="line-break"></div>',
        link: function(scope, element, attrs) {
            if (element[0].previousSibling.className === 'line-break') {
                element.css('display', 'block');
                element.css('height', '15px');
            }
        }
    };
});