angular.module('RevisionApp')
.directive('edit', function($timeout, $compile, activeInputService, templateMapService, $rootScope, sortedService) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            
            var dragging, textareas;
            scope.$on('draggable:start', function() {
                dragging = true;
            });
            
            scope.$on('draggable:end', function() {
                $timeout(function() {
                    dragging = false;
                }, 10);
            });
            
            scope.$on('remove', function(event, args) {
                if (element.attr('edit-content-element') > args.elCount) {
                    element.attr('edit-content-element', element.attr('edit-content-element') -1);
                }
            });
            
            var elementNumber, insertion;
            //function for reordering when element is being moved backwards
            function reorderBack(moveElNumber, targetElNumber) {
                 //cancel listeners
                reorderForwardListener();
                reorderBackListener();
                //get element number
                elementNumber = Number(element.attr('edit-content-element'));
                //if the element is between the element source and target
                if (elementNumber < moveElNumber && elementNumber >= targetElNumber) {
                    //increase the element number by 1
                    element.attr('edit-content-element', Number(element.attr('edit-content-element'))+1);
                //else if this the element being moved backwards
                } else if (elementNumber === moveElNumber) {
                    //set the element number to the target number
                    element.attr('edit-content-element', targetElNumber);
                    //set insertion pointhow 
                    insertion = angular.element(document.querySelectorAll('[edit-content-element="'+(targetElNumber-1).toString()+'"]'));
                    //if moving to the first element
                    if (isNaN(insertion.attr('edit-content-element'))) {
                        //prepend the parent, as not possible to insert after anything
                        element.parent().prepend(element);
                    } else {
                        //else insert after insertion
                        insertion.after(element);
                    }
                }
            }
            
            function reorderForward(moveElNumber, targetElNumber) {
                //cancel listeners
                reorderForwardListener();
                reorderBackListener();

                elementNumber = Number(element.attr('edit-content-element'));
                //if the element is between the source and the target
                if (elementNumber > moveElNumber && elementNumber < targetElNumber) {
                    //decrease the element number by 1
                    element.attr('edit-content-element', Number(element.attr('edit-content-element'))-1);   
                //else if the element is the one being moved
                } else if (elementNumber === moveElNumber) {
                    //set insertion point before setting the content element
                    insertion = angular.element(document.querySelectorAll('[edit-content-element="'+(targetElNumber-1).toString()+'"]'));
                    //set the element number to 1 less than the target (as moving before)
                    element.attr('edit-content-element', targetElNumber-1);
                    //insert after insertion
                    insertion.after(element);
                }
            }
            
            //on reordering of this element number
            var reorderBackListener = scope.$on('reorderBack'+element.attr('edit-content-element'), function(event, args) {
                //run function to reorder
                reorderBack(args.moveElNumber, args.targetElNumber);
            });
            
            
            //on reordering of this element number
            var reorderForwardListener = scope.$on('reorderForward'+element.attr('edit-content-element'), function(event, args) {
                //run function to reorder
                reorderForward(args.moveElNumber, args.targetElNumber);
            });
            
            scope.$on('reassignOrderingFunctions', function() {
                //reassign        
                 reorderBackListener = scope.$on('reorderBack'+element.attr('edit-content-element'), function(event, args) {
                    //run function to reorder
                    reorderBack(args.moveElNumber, args.targetElNumber);
                });
                //reassign
                 reorderForwardListener = scope.$on('reorderForward'+element.attr('edit-content-element'), function(event, args) {
                    //run function to reorder
                    reorderForward(args.moveElNumber, args.targetElNumber);
                });
            });
            
            element.on('click', function() {
                if (activeInputService.get() === false && !dragging) {
                    //get template
                    var el = angular.element(templateMapService.editInputs[element.attr('type')]);
                    el.attr('edit-content-element', element.attr('edit-content-element'));
                    //create con
                    var elCon = angular.element('<div></div>');
                    elCon.append(angular.copy(element));
                    //append previous element string to the element as a attribute-property for edit cancel
                    el.attr('prev', elCon.html());
                    
                    //if image 
                    if (element.attr('type') === 'image') {
                        //get and set uuid
                        el.attr('uuid',element.attr('uuid'));
                        //get base64Data
                        var base = element.attr(`src`);
                        //set base64Data
                        el.attr('base', base.slice(23, base.length));
                    }
                    //question
                    if (element.attr('type') === 'question') {
                        //get the textareas
                        textareas = el.children().eq(0).find('textarea');
                        //get question and answers (total of 5) text
                        var inputTexts = element.children();
                        //insert question text
                        angular.element(textareas[0]).html(angular.element(inputTexts[0]).html()); 
                        //insert answer texts
                        for (var x=1; x < inputTexts.length; x++) {
                            angular.element(textareas[x]).html(angular.element(inputTexts[x]).children().eq(0).html()); 
                            //if correct answer
                            if (angular.element(inputTexts[x]).attr('correct')) {
                                var correct = x;
                            }
                        }
                        
                        //select the correct answer checkbox
                        var checkboxes = el.children().eq(1).find('md-checkbox');
                        angular.element(checkboxes[correct-1]).addClass('md-checked');

                    //definition    
                    } else if (element.attr('type') === 'definition') {
                        //get the textareas
                        textareas = el.find('textarea');
                        //get the input text for definable
                        var inputTextDefinable = element.children().eq(0).html();
                        //get the input text for definition
                        var inputTextDefinition = element.children().eq(1).html();
                        //remove the colon
                        inputTextDefinition = inputTextDefinition.slice(2, inputTextDefinition.length);
                        //insert definable into first textarea
                        angular.element(textareas[0]).html(inputTextDefinable);
                        //insert definition into second text area
                        angular.element(textareas[1]).html(inputTextDefinition);
                        
                    } else if (element.attr('type') === 'code') {
                        textareas = el.find('textarea');
                        var input = element.children().eq(0).children().eq(1).html();
                        var output = element.children().eq(1).children().eq(1).html();
                        input = input.replaceAll('<br>', '\n');
                        output = output.replaceAll('<br>', '\n');
                        angular.element(textareas[0]).html(input);
                        angular.element(textareas[1]).html(output);
                        
                    //single input content types    
                    } else {
                        //get the textarea
                        var textarea = el.find('textarea');
                        //get the input text
                        var inputText = element.html();
                        //replace all <br> elements with newlines
                        inputText = inputText.replaceAll('<br></br>', '\n');
                        //insert text into input field for editing
                        textarea.html(inputText);
                    }
                    //compile
                    $compile(el)(scope);
                    //set activeInput to true
                    activeInputService.set(true);
                    //remove event listeners
                    reorderForwardListener();
                    reorderBackListener();
                    //replace element
                    element.replaceWith(angular.element(el));
                }
            });
    
        }
    };
});