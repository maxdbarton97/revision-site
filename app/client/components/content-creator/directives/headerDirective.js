angular.module('RevisionApp')
.directive('pageHeader', function($location, $compile) {
    return {
        restrict: 'E',
        link: function(scope, element, attrs) {
            
            var el = angular.element('<div title-case class="page-header"></div>');
            var splits = $location.url().split('/');
            var header = splits[splits.length-1];
            header = header.replaceAll('-', ' ');
            
            el.html(header);
            $compile(el)(scope);
            element.append(el);
            
            String.prototype.replaceAll = function(search, replacement) {
                var target = this;
                return target.split(search).join(replacement);
            };
    
        }
    };
});