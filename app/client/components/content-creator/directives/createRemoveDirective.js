/*
 * Directive that is applied to a new elements remove button, that creates a 
 * listener on the element so when clicked, the element is removed.
 */

angular.module('RevisionApp')
.directive('createRemove', function(activeInputService) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            
            //=================================================================
            // listeners 
            //=================================================================
            
            element.on('click', function() {
                activeInputService.set(false);
                element.parent().remove();
            });
            
        }
    };
});