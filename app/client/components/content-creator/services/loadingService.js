angular.module('RevisionApp')
.service('loadingService', function() {
    
    var loadingSpinnerTemplate = '<img src="/app/client/images/spinner.gif" class="loading-spinner">';
    
    this.start = function(element){
        var loadingSpinner = angular.element(angular.copy(loadingSpinnerTemplate));
        loadingSpinner.attr('uuid', element.attr('uuid'));
        element.parent()[0].insertBefore(loadingSpinner[0], element[0]);
    };
    
    this.end = function(element){
        var loading = element.parent().find('img');
        for (var x = 0; x < loading.length; x++) {
            if (angular.element(loading[x]).attr('uuid') === element.attr('uuid') && 
            angular.element(loading[x]).hasClass('loading-spinner')) {
                loading[x].remove();
            }
        }
    };
    
});