angular.module('RevisionApp')
.service('sortedService', function() {
    
    let checkList = {};
    this.checkIfSorted = function(number) {
        if (checkList[number]) return false;
        else return true;
    };
    
    this.checked = function(number) {checkList[number] = true};
    
    this.resetChecklist = function() {
        checkList = {};  
    };
    
});