angular.module('RevisionApp')
    .service('contentElementCountService', function($location, $document, $rootScope, $route, $timeout) {
    
    var count = angular.element($document[0].querySelectorAll("[content-element]")).length;
    
    //something to watch when the page changes
    $rootScope.$on('$routeChangeSuccess', function() {
        var template = angular.element($route.current.locals.$template);
        count = angular.element(template[0].querySelectorAll("[content-element]")).length;
    });

    this.add = function() {
        count = Math.abs(count-0);
        count = count + 1;
    };
    
    this.subtract = function() {
        count = Math.abs(count-0);
        count = count - 1;
    };
    
    this.get = function() {
        return count;
    };
    
});