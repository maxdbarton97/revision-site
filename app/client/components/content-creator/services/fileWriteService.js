angular.module('RevisionApp')
.service('fileWriteService', function($route, $http, activeInputService, $timeout, contentElementCountService, $rootScope, templateMapService) {
 
 
    let lineNumber;
    
    //function for getting last line
    function getLastElementLine(array, string) {
        var line;
        array.forEach(function(value, index) {
            if (value.includes(string)) {
                line = index;
            }
        });
        return (line+1);
    }
    
    //initially define template
    let template = $route.current.locals.$template;
    //if route changes, update template
    $rootScope.$on('$routeChangeSuccess', function() {
        template = $route.current.locals.$template;
    });
    
    //function for writing
    function write() {
        
        // remove any instances of ng-transclude that may have been included
        // when compiling directives, so that transclude duplication doesn't
        // occur
        while (true) {
        	if (template.includes(' ng-transclude=""')) {
          	template = template.replace(' ng-transclude=""', '');
          } else break;
        }
    
        $http.post('/write', {
            template: template,
            filePath: `app/client/${$route.current.loadedTemplateUrl}`,
        }).then(function(res) {
            $timeout(function(){
                activeInputService.set(false);
            }, 2000);
        });
    }
    
    this.create = function(elementText) {
        //split into an array of lines
        template = template.split('\n');
        //get line number
        lineNumber = getLastElementLine(template, '<content>') - 1;
        //insert
        template.splice(lineNumber, 0, elementText);
        //join array into string
        template = template.join("\n");
        //post
        write();
        //add to the element count
        contentElementCountService.add();
        
    };
    
    this.delete = function(elementNumber) {
        //broadcast to edit elements to change their attribute values
        $rootScope.$broadcast('remove', {elCount: elementNumber});
        //split into an array of lines
        template = template.split('\n');
        //get the line number to remove
        var lineToRemove = getLastElementLine(template, 'content-element="' + elementNumber + '"');
        //remove (-1 due to index starting at 0 not 1)
        template.splice(lineToRemove-1, 1);
        //change all content-element values for remaining content elements
        //get last line
        var lastElementLine = getLastElementLine(template, '<content>') - 1;
        //slice template to get elements to change
        var elementsToChange = template.slice(lineToRemove-1, lastElementLine);
        //loop through and change values
        //variable used to set new value
        var newValue = Number(elementNumber);
        //variable used for assigning the new string values in the template array
        var templateLine = lineToRemove -1;
        var value;
        elementsToChange.forEach(function(string, index) {
            //replace old value with new                  E.G value 7                                           becomes value 6
            value = string.replace('content-element="' + (newValue + 1).toString() + '"', 'content-element="' + newValue.toString() + '"');
            //replace template with updated string
            template[templateLine] = value;
            //incriment values for next line
            newValue += 1;
            templateLine += 1;
        });
        //convert array into string and write
        template = template.join("\n");
        //write
        write();
        //subtract content element number
        contentElementCountService.subtract();
    };
    
    this.edit = function(elementText, elementNumber) {
        template = template.split('\n');
        lineNumber = getLastElementLine(template, 'content-element="'+(elementNumber).toString()+'"');
        template[lineNumber-1] = elementText;
        template = template.join("\n");
        write();
    };
    
    this.reorder = function(editElements) {
        template = template.split('\n');
        lineNumber = getLastElementLine(template, 'content-element="0"');
        var el, elCon;
        for(var x = 0; x < editElements.length; x++) {
            el = angular.element(templateMapService.finals[angular.element(editElements[x]).attr('type')]);
            el.html(angular.element(editElements[x]).html());
            el.attr('content-element', angular.element(editElements[x]).attr('edit-content-element'));
            elCon = angular.element('<div></div>');
            elCon.append(el);
            template[lineNumber+x-1] = `    ${elCon.html()}`;
        }
        template = template.join("\n");
        write();
    };
    

});