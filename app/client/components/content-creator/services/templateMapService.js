angular.module('RevisionApp')
    .service('templateMapService', function() {
    
    //edit renders
    this.edits = {
        'text': '<div edit ng-drag="drag" ng-drop="true" ng-drop-success="drop($event)" ng-center-anchor="true" ng-controller="contentElementCtrl" type="text" class="text edit"></div>',
        'subheader': '<div edit ng-drag="drag" ng-drop="true" ng-drop-success="drop($event)" ng-center-anchor="true" ng-controller="contentElementCtrl" type="subheader" title-case class="subheader md-headline edit"></div>',
        'definition': '<div edit ng-drag="drag" ng-drop="true" ng-drop-success="drop($event)" ng-center-anchor="true" ng-controller="contentElementCtrl" type="definition" class="edit definition-container"><div class="definable"></div><div></div></div>',
        'code': '<div edit ng-drag="drag" ng-drop="true" ng-drop-success="drop($event)" ng-center-anchor="true" ng-controller="contentElementCtrl" type="code" class="edit"><div class="code-input-container"><div class="container-label">Python</div><pre></pre></div><div class="code-output-container"><div class="container-label">Output</div><div></div></div></div>',
        'list': '<div edit type="list" ng-drag="drag" ng-drop="true" ng-drop-success="drop($event)" ng-center-anchor="true" ng-controller="contentElementCtrl" class="medium-padding large-padding-left edit"></div>',
        'question': `<div edit type="question" layout="column" class="question-container edit"><div class="question"></div><md-button class="answer" layout layout-align="center center"><div flex class="large-padding-left"></div><md-icon class="material-icons answer-indicator ng-hide">close</md-icon></md-button><md-button class="answer" layout layout-align="center center"><div flex class="large-padding-left"></div><md-icon class="material-icons answer-indicator ng-hide">close</md-icon></md-button><md-button class="answer" layout layout-align="center center"><div flex class="large-padding-left"></div><md-icon class="material-icons answer-indicator ng-hide">close</md-icon></md-button><md-button class="answer" layout layout-align="center center"><div flex class="large-padding-left"></div><md-icon class="material-icons answer-indicator ng-hide">close</md-icon></md-button></div>`,
        'image': '<img image edit ng-drag="drag" ng-drop="true" ng-drop-success="drop($event)" ng-center-anchor="true" ng-controller="contentElementCtrl" type="image" class="image edit">',
        'source': '<div edit ng-drag="drag" ng-drop="true" ng-drop-success="drop($event)" ng-center-anchor="true" ng-controller="contentElementCtrl" type="source" class="source edit"></div>'
    }; 
    
    //final renders
    this.finals = {
        'text': '<div type="text" class="text"></div>',
        'subheader': '<div type="subheader" title-case class="md-headline subheader"></div>',
        'definition': '<div type="definition" class="definition-container"><div class="definable"></div><div></div></div>',
        'code': '<div type="code"><div class="code-input-container"><div class="container-label">Python</div><pre></pre></div><div class="code-output-container"><div class="container-label">Output</div><div></div></div></div>',
        'list': '<div type="list" class="medium-padding large-padding-left edit"></div>',
        'question': `<div type="question" layout="column" class="question-container" ng-controller="questionController"><div class="question"></div><md-button ng-click="answerSelect($event)" ng-class="{'answer-hover': answered === false}" class="answer" ng-disabled="answered" layout layout-align="center center"><div flex class="large-padding-left"></div><md-icon class="material-icons answer-indicator ng-hide">close</md-icon></md-button><md-button ng-click="answerSelect($event)" ng-class="{'answer-hover': answered === false}" class="answer" ng-disabled="answered" layout layout-align="center center"><div flex class="large-padding-left"></div><md-icon class="material-icons answer-indicator ng-hide">close</md-icon></md-button><md-button ng-click="answerSelect($event)" ng-class="{'answer-hover': answered === false}" class="answer" ng-disabled="answered" layout layout-align="center center"><div flex class="large-padding-left"></div><md-icon class="material-icons answer-indicator ng-hide">close</md-icon></md-button><md-button ng-click="answerSelect($event)" ng-class="{'answer-hover': answered === false}" class="answer" ng-disabled="answered" layout layout-align="center center"><div flex class="large-padding-left"></div><md-icon class="material-icons answer-indicator ng-hide">close</md-icon></md-button></div>`,
        'image': '<img image type="image" class="image">',
        'source': '<div  type="source" class="source"></div>'

    };
    
    //input fields
    this.createInputs = {
        'subheader':
            `<div class="creator-input" type="subheader" layout layout-align="center center">
                <textarea flex rows="1"></textarea>
                <md-button create-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button create-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
            
        'text': 
            `<div class="creator-input" type="text" layout layout="center center">
                <textarea flex rows="4"></textarea>
                <md-button create-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button create-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
            
        'definition': 
            `<div class="creator-input" type="definition" layout layout="center center">
                <textarea flex rows="1" placeholder="definable"></textarea>
                <textarea flex rows="3" placeholder="definition"></textarea>
                <md-button create-save type="text" class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button create-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
        
        'code': 
            `<div class="creator-input" type="code" layout layout="center center">
                <textarea flex rows="4" placeholder="input"></textarea>
                <textarea flex rows="4" placeholder="output"></textarea>
                <md-button create-save type="text" class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button create-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
            
        'list': 
            `<div class="creator-input" type="list" layout layout="center center">
                <textarea flex rows="5">• </textarea>
                <md-button create-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button create-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
            
        'image': 
            `<div class="creator-input" type="image" layout layout="center center">
                <div create-image ngf-drop ngf-select ng-model="image" class="image-drop-box" 
                ngf-drag-over-class="'dragover'" ngf-allow-dir="true" accept="image/*" 
                ngf-pattern="'image/*'">Drop image here or click to upload</div>
                <md-button create-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button create-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
            
        'question': 
            `<div class="creator-input" type="question" layout layout="center center">
                <div layout="column" flex="65" layout-align="center stretch">
                    <textarea rows="1" placeholder="Question..."></textarea>
                    <textarea rows="1" placeholder="Answer 1"></textarea>
                    <textarea rows="1" placeholder="Answer 2"></textarea>
                    <textarea rows="1" placeholder="Answer 3"></textarea>
                    <textarea rows="1" placeholder="Answer 4"></textarea>
                </div>
                <div layout="column" flex="25" layout-align="start stretch">
                    <div class="answer-checkbox">Correct Answer</div>
                    <md-checkbox class="answer-checkbox"></md-checkbox>
                    <md-checkbox class="answer-checkbox"></md-checkbox>
                    <md-checkbox class="answer-checkbox"></md-checkbox>
                    <md-checkbox class="answer-checkbox"></md-checkbox>
                </div>
                <md-button create-save flex="5" class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button create-remove flex="5" class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
                
        'source': 
            `<div class="creator-input" type="source" layout layout="center center">
                <textarea flex rows="1"></textarea>
                <md-button create-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button create-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`
            
        
    };
    
    this.editInputs = {
        'subheader':
            `<div class="creator-input" type="subheader" layout layout-align="center center">
                <textarea flex rows="1"></textarea>
                <md-button edit-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button edit-cancel class="md-icon-button">
                    <md-icon>close</md-icon>
                </md-button>
                <md-button edit-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
            
        'text': 
            `<div class="creator-input" type="text" layout layout="center center">
                <textarea flex rows="4"></textarea>
                <md-button edit-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button edit-cancel class="md-icon-button">
                    <md-icon>close</md-icon>
                </md-button>
                <md-button edit-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
        
        'definition': 
            `<div class="creator-input" type="definition" layout layout="center center">
                <textarea flex rows="1" placeholder="definable"></textarea>
                <textarea flex rows="3" placeholder="definition"></textarea>
                <md-button edit-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button edit-cancel class="md-icon-button">
                    <md-icon>close</md-icon>
                </md-button>
                <md-button edit-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
            
        'code': 
            `<div class="creator-input" type="code" layout layout="center center">
                <textarea flex rows="4" placeholder="input"></textarea>
                <textarea flex rows="4" placeholder="output"></textarea>
                <md-button edit-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button edit-cancel class="md-icon-button">
                    <md-icon>close</md-icon>
                </md-button>
                <md-button edit-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
        
        'list': 
            `<div class="creator-input" type="list" layout layout="center center">
                <textarea flex rows="5"></textarea>
                <md-button edit-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button edit-cancel class="md-icon-button">
                    <md-icon>close</md-icon>
                </md-button>
                <md-button edit-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
            
        'image': 
            `<div class="creator-input" type="image" layout layout="center center">
                <div flex>Image Options</div>
                <md-button edit-cancel class="md-icon-button">
                    <md-icon>close</md-icon>
                </md-button>
                <md-button edit-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
        
        'question': 
            `<div class="creator-input" type="question" layout layout="center center">
                <div layout="column" flex="65" layout-align="center stretch">
                    <textarea rows="1" placeholder="Question..."></textarea>
                    <textarea rows="1" placeholder="Answer 1"></textarea>
                    <textarea rows="1" placeholder="Answer 2"></textarea>
                    <textarea rows="1" placeholder="Answer 3"></textarea>
                    <textarea rows="1" placeholder="Answer 4"></textarea>
                </div>
                <div layout="column" flex="25" layout-align="start stretch">
                    <div class="answer-checkbox">Correct Answer</div>
                    <md-checkbox class="answer-checkbox"></md-checkbox>
                    <md-checkbox class="answer-checkbox"></md-checkbox>
                    <md-checkbox class="answer-checkbox"></md-checkbox>
                    <md-checkbox class="answer-checkbox"></md-checkbox>
                </div>
                <md-button edit-save flex="5" class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button edit-cancel flex="5" class="md-icon-button">
                    <md-icon>close</md-icon>
                </md-button>
                <md-button edit-remove flex="5" class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`,
            
        'source': 
            `<div class="creator-input" type="source" layout layout="center center">
                <textarea flex rows="4"></textarea>
                <md-button edit-save class="md-icon-button">
                    <md-icon>check</md-icon>
                </md-button>
                <md-button edit-cancel class="md-icon-button">
                    <md-icon>close</md-icon>
                </md-button>
                <md-button edit-remove class="md-icon-button">
                    <md-icon>delete</md-icon>
                </md-button>
            </div>`
    };
    
});