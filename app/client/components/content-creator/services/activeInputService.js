angular.module('RevisionApp')
    .service('activeInputService', function() {
        
        this.active = false;
        
        this.set = function(val) {
            this.active = val;
        };
        
        this.get = function() {
            return this.active;
        };
});