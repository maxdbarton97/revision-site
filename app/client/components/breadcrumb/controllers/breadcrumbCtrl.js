/*eslint-disable no-extend-native, no-constant-condition, no-undef*/

/*
 * The breadcrumb service controller is responsible for handling the business 
 * logic end of the breadcrumb directive.
 */

angular.module('RevisionApp')
.controller('breadcrumbCtrl', function(
    $location, 
    $scope, 
    $mdSidenav, 
    $mdComponentRegistry
) {
    
    //=========================================================================
    // scope variables 
    // ========================================================================
    
    $scope.breadcrumb = false;
    $scope.breadcrumbMenu = false;
    $scope.margin = false;
    
    //=========================================================================
    // scope functions 
    // ========================================================================
    
    // watch for when the URL changes, so the breadcrumb can be updated
    $scope.$watch(function() {
        return $location.url();
    }, function(val) {
        var nodes = val.split('/');
        while (true) {
            if (nodes.includes('')) {
                nodes.splice(nodes.indexOf(''), 1);
            } else {
                break;
            }
        }
        if (nodes.length > 0) {
            $scope.$parent.breadcrumb = true;
        } else {
            $scope.$parent.breadcrumb = false;
        }
    });
    
    // update the margin varaible when the sidenav closes or opens,
    // so the main view can apply a margin
    $mdComponentRegistry.when('left').then(function() {
        $scope.$watch(function() {
            return $mdSidenav('left').isOpen();
        }, function(val) {
            $scope.margin = val;
        });
    });
    
    // when a breadcrumb element is selected, take the string and use it to
    // for the new url 
    $scope.direct = function(str) {
        var leaf = str.toLowerCase();
        leaf = leaf.replaceAll(' ', '-');
        var path = $location.url();
        if (path.indexOf(`/${leaf}/`) >= 0) {
            path = path.slice(0, Number(path.indexOf(`/${leaf}/`))) + '/' + leaf;
            $location.url(path);
        } else {
            return;
        }
    };
    
    //=========================================================================
    // helper functions 
    // ========================================================================
        
    // used to replace the spaces with dashes when converting the URL
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };
    
});