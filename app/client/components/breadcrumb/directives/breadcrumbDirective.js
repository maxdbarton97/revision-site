
/*
 * The breadcrumb directive is used as one of the apps main components, and
 * takes the current instance of the URL to convert it into a readable 
 * breadcrumb, which the user is then able to use to determine where they 
 * are.
 * 
 * They are also able to select each breadcrumb element to view the page
 * for it.
 */

angular.module('RevisionApp')
.directive('breadcrumb', function(
    $compile,
    $location,
    $rootScope,
    $timeout,
    $window,
    titleCaserService
) {
    return { link: function(scope, element, attrs) {
        
        //=====================================================================
        // variables
        //=====================================================================
        
        // required HTML elements
        var breadcrumbInsertion = angular.element(document.getElementById('breadcrumb-insertion'));
        var menuInsertion = angular.element(document.getElementById('breadcrumb-menu-insertion'));
        
        //exceptions for the breadcrumb, for example if includes commas or hyphons
        const exceptions = [
            'individual social ethical legal and cultural issues and oppertunities',
            'graph traversal',
            'tree traversal',
            'context free languages',
            'representing images sounds and other data'
        ];
        // corresponding replacements, must keep in correct order!
        const exceptionReplacements = [
            'Individual, Social, Ethical, Legal and Cultural Issues and Oppertunities',
            'Graph-Traversal',
            'Tree-Traversal',
            'Context-Free Languages',
            'Representing Images, Sounds, and Other Data'
        ];

        // templates
        var chevron = angular.element('<md-icon" class="material-icons">chevron_right</md-icon>');
        var breadcrumbElement = angular.element('<div class="breadcrumb-element"></div>');
        var menuElement = angular.element('<md-menu-item><md-button></md-button></md-menu-item>');
        var currentElement, currentChevron, overflown;
        
        var nodesGlobal, availableWidth;
                
        //=====================================================================
        // init
        //=====================================================================  
        
        // once the first digest cycle has completed, register a watch function
        $timeout(function() {
            $rootScope.$watch(function() {
                return $location.url();
            }, function(val) {
                var nodes = val.split('/');
                while (true) {
                    if (nodes.indexOf('') > -1) {
                        nodes.splice(nodes.indexOf(''), 1);
                    } else break;
                }

                nodesGlobal = nodes.reverse();
                createBreadcrumb();
            }); 
        }, 50);
        
        angular.element($window).on('resize', function() {
            createBreadcrumb();
        });
        
        //=====================================================================
        // init
        //=====================================================================
        
        // the function which create the actual breadcrumb
        function createBreadcrumb() {
            availableWidth = $window.innerWidth - 75;
            breadcrumbInsertion.empty();
            menuInsertion.empty();
            scope.breadcrumbMenu = false;
            overflown = false;
            
            angular.forEach(nodesGlobal, function(value, index) {
                if (!overflown) {
                    currentChevron = angular.copy(chevron);
                    currentElement = angular.copy(breadcrumbElement);
                    currentElement.data('raw-str', value);
                    currentElement.attr('ng-click', 'direct("' + value + '")');
                    value = value.replaceAll('-', ' ');
                    value = titleCaserService.titleCaser(value);
                    if (exceptions.indexOf(value) > -1) {
                        value = exceptionReplacements[exceptions.indexOf(value)];
                    }
                    currentElement.html(value);
                    breadcrumbInsertion.prepend(currentElement);
                    breadcrumbInsertion.prepend(currentChevron);
                    
                    availableWidth -= (currentElement[0].offsetWidth + 24);
                    
                    // if now overflown
                    if (availableWidth < 0) {
                        overflown = true;
                        scope.breadcrumbMenu = true;
                        if (index === 0) {
                            availableWidth += (currentElement[0].offsetWidth + 24);
                            var x = 0;
                            while(true) {
                                x += 1;
                                currentElement.html(value.slice(0, value.length - x) + '...');
                                availableWidth -= (currentElement[0].offsetWidth + 24);
                                if (availableWidth >= 0) break;
                                else {
                                    availableWidth += (currentElement[0].offsetWidth + 24);
                                } 
                            }
                        } else {
                            currentElement.remove();
                            currentChevron.remove();
                        }
                    }
                }
                if (overflown && index > 0) {
                    value = value.replaceAll('-', ' ');
                    currentElement = angular.copy(menuElement);
                    currentElement.children().eq(0).data('raw-str', value);
                    currentElement.children().eq(0).attr('ng-click', 'direct("' + value + '")');
                    currentElement.children().eq(0).attr('title-case', '');
                    currentElement.children().eq(0).addClass('invisible');
                    currentElement.children().eq(0).html(value);
                    menuInsertion.prepend(currentElement);
                }
            });
            
            $compile(breadcrumbInsertion.contents())(scope);
            if (overflown) $compile(menuInsertion.contents())(scope);
            if (!scope.$$phase) scope.$apply();
            
        }
        
        String.prototype.replaceAll = function(search, replacement) {
            var target = this;
            return target.split(search).join(replacement);
        };
            
    }}
})