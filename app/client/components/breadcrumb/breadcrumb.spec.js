describe('breadcrumb', function() {
    beforeEach(module('RevisionApp'));
      
    var breadcrumb, location;
    beforeEach(function() { 
        inject(function($compile, $rootScope, $location, _$timeout_){
            $timeout = _$timeout_;
            compile = $compile;
            scope = $rootScope.$new();
            location = $location;
        });
        
        breadcrumb = angular.element('<div breadcrumb></div>');
        breadcrumb = compile(breadcrumb)(scope);
        scope.$digest();
    
    });
        
    describe('Breadcrumb Directive', function() {
        it('should take the url and convert it into a chevron seperated breadcrumb',function(){
            var path = 'computer-science/aqa/unit-1';
            location.url(path);
            scope.$apply();
            angular.forEach(breadcrumb.children(), function(value, index) {
                //if the childs index is even
                if (index % 2 === 0) {
                    expect(breadcrumb.children().eq(index).children().eq(0).html())
                    .toBe('chevron_right');
                } else {
                    expect(breadcrumb.children().eq(index).data('rawStr')).toBe(path.split('/')[(index-1)/2]);
                }
            });
        });
    });
});