angular.module('RevisionApp')
    .service('testService', function() {
        
        this.correctAnswers = 0;
        
        this.answer = function(ans) {
            if (ans) {
                this.correctAnswers++;
            }
        };
        
        this.mark = function() {
            
        };
});