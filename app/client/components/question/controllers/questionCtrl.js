angular.module('RevisionApp')
.controller('questionController', function($scope, $element, testService) {
    
    $scope.answered = false;

    $scope.answerSelect = function(event) {
        //if test
        if ($element.attr('test') === 'true') {
           //remove all highlights
           for (var x=1; x < $element.children().length; x++) {
               $element.children().eq(x).removeClass('highlight-answer');
           }
           //add highlight to answer
            angular.element(event.srcElement).parent().addClass('highlight-answer');
            
        //if topic question
        } else {
            //if correct
            //get element
            var el = angular.element(event.srcElement).parent();
            if (el.attr('correct') === 'true') {
                $scope.$broadcast('correct');
                el.addClass('correct-answer');
            //if incorrect
            } else {
                el.addClass('incorrect-answer');
            }
            //remove hide on answer indicator
            el.children().eq(1).removeClass('ng-hide');
            //disable answers
            $scope.answered = true;
        }

    $scope.$on('disable-answers', function() {
        //disable answers
        $scope.answered = true;
        //display icons
        var answers = $element[0].getElementsByClassName('answer');
        for (var x = 0; x < answers.length; x++) {
            if (angular.element(answers[x]).hasClass('highlight-answer')) {
                if (angular.element(answers[x]).attr('correct') === 'true') {
                    $scope.$broadcast('correct');
                }
                angular.element(answers[x]).children().eq(1).removeClass('ng-hide');
            }
        }
        
    });
    
    };
})