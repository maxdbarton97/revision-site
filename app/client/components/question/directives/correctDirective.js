angular.module('RevisionApp')
.directive('correct', function($document, $location) {
    return {
        link: function(scope, element, attrs) {

            scope.$on('correct', function() {
                var icon = element.find('md-icon');
                icon.html('check');
            });
            
        }
    };
});