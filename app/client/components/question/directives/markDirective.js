angular.module('RevisionApp')
.directive('markButton', function($document, $location, $rootScope, $route, $window) {
    return {
        template: `<div layout layout-align="start start" class="large-padding-top large-margin-top large-padding-bottom large-margin-bottom">
                        <md-button flex="25" class="mark-button">{{buttonText}}</md-button>
                        <div ng-show="incomplete" class="mark-text">Please answer all of the questions to mark your test.</div> 
                        
                        <div flex="75" layout="column" layout-align="center center" ng-show="marked">
                            <div class="mark-text">You scored: {{mark}}/{{total}}</div>
                            <div class="mark-text">Percentage: {{percentage}}%</div>
                            <div class="mark-text">Grade: {{grade}}</div>

                        </div>
                    </div>`,
                    
        link: function(scope, element, attrs) {

            //grade boundries
            function getGrade(percentage) {
                //A*
                if (percentage >= 90) {
                    return 'A*';
                }
                //A
                if (percentage >= 80) {
                    return 'A';
                }
                //B
                if (percentage >= 70) {
                    return 'B';
                }
                //C
                if (percentage >= 60) {
                    return 'C';
                }
                //D
                if (percentage >= 50) {
                    return 'D';
                }
                //E
                if (percentage >= 40) {
                    return 'E';
                }
                return 'U';
            }
            
            scope.incomplete = false;
            scope.marked = false;
            scope.buttonText = 'Mark Test';
            
            element.on('click', function(event) {
                
                //get document
                var parent = element.parent();
                //get questions
                var questions = parent[0].getElementsByClassName('question-container');
                //get answers
                var answers = parent[0].getElementsByClassName('highlight-answer');

                if (questions.length !== answers.length) {
                    scope.incomplete = true;
                    scope.$apply();
                } else {
                    scope.incomplete = false;
                    scope.$apply();
                    
                    //total
                    scope.total = questions.length;
                    
                    //mark
                    scope.mark = 0;
                    for(var x = 0; x < answers.length; x++) {
                        if (angular.element(answers[x]).attr('correct') === 'true') {
                            scope.mark += 1;
                            angular.element(answers[x]).addClass('correct-answer');
                        } else {
                            angular.element(answers[x]).addClass('incorrect-answer');
                        }
                    }
                    
                    //disable answers
                    $rootScope.$broadcast('disable-answers');
                    
                    //percentage
                    scope.percentage = Math.round((scope.mark/scope.total) * 100);
                    
                    //grade 
                    scope.grade = getGrade(scope.percentage);
                    
                    //show and apply
                    scope.marked = true;

                    //re-purpose button
                    scope.buttonText = 'Take Again';
                    scope.$apply();
                    
                    element.parent().parent().parent()[0].scrollTop = 10000;

                    
                    element.on('click', function() {
                        element.parent().parent().parent()[0].scrollTop = 0;
                        $route.reload();
                    });

                    
                }
            });
        }
            
    };     
});