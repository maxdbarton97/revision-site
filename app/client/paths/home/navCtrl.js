angular.module('RevisionApp')
.controller('navCtrl', function($scope, $timeout, $window, $location, $route, $element) {
    
    $scope.direct = function(e) {
        angular.element(e.target).addClass('no-pointer-events');
        var str = e.srcElement.textContent.toLowerCase().replaceAll(' ', '-');
        str = str.replaceAll(',', '');
        $location.url($location.url() + '/' + str);
    };
    
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };
    
    //resolve issue where buttons appeared pressed when a page has loaded
    //blur the activeElement when the document
    $element.ready(function() {
        var activeElement = document.activeElement;
        activeElement.blur();
    });
    
})