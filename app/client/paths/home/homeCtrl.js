angular.module('RevisionApp')
.controller('homeCtrl', function($scope, $mdSidenav, $location, $anchorScroll, $timeout) {
    
    $scope.openSidenav = function() {
        $mdSidenav('left').open();
    };
    
    $scope.goToBottom = function() {
        $location.hash('more-anchor');
        $anchorScroll();
        $timeout(function() {
            $location.hash('');
        });
    };
    
});
