describe('homeCtrl', function() {
    beforeEach(module('RevisionApp'));
    
    var $controller;

    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
    }));
    
    describe('message', function() {
        it('should read hello', function() {
            var $scope = {};
            var controller = $controller('homeCtrl', { $scope: $scope });
            expect($scope.message).toBe('Hello');
        });
    });
    
});