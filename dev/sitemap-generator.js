var exec = require('child_process').exec;

var sitemap = [];
var results = {};
var counters = [];

function execute(str, res, path) {
    exec(str, function(error, stdout, stderr) {
        var results = stdout.split('\n');
        results.splice(-1,1);
        res([results, path]);
    });
}

//var domain = 'http://www.alvls.com';

function treeTraverser(path) {
    var treeTraversePromise = new Promise(function(res, rej) {
        children = execute('cd ' + path + '&& ls', res, path);
    });
    return treeTraversePromise;
}


function treeTraverserTop(path) {
    var topLevelPromise = new Promise(function(topRes, rej) {
        treeTraverser(path).then(function(array) {
            results[array[1]] = array[0];
            counters[path] = 0;
            function iterateCheck() {
                checkChild(results[array[1]][counters[path]], counters[path], array[1]).then(function(){
                    if (counters[path] !== results[array[1]].length-1) {
                        counters[path] += 1;
                        itterateCheck();
                    } else {
    
                        topRes(true);
                    }
                });
            }
            iterateCheck();
        });
    });
    return topLevelPromise;
}

function checkChild(value, index, path) {
    var checkChildPromise = new Promise(function(res, rej) {
        if (value.includes('.html')) {
            sitemap.push(path + '/' + value);
            res(true);
        } else {
            treeTraverser(path + '/' + value).then(function(array) {
                results[array[1]] = array[0];
                counters[path] = 0;
                
                function iterateCheck() {
                    checkChild(results[array[1]][counters[path]], counters[path], array[1]).then(function(){
                        if (counters[path] !== results[array[1]].length-1) {
                            counters[path] += 1;
                            iterateCheck();
                        } else {
                            res(true);
                        }
                    });
                }
                iterateCheck();
            });
        }
    });
    return checkChildPromise;
}


treeTraverserTop('app/client/paths').then(function() {
    
    var text = '';
    
    //URLify
    sitemap.forEach(function(value, index) {
        sitemap[index] = 'http://www.alvls.com/' + value.slice(17, value.lastIndexOf('/'));
        text += sitemap[index] + '\n';
    });
    
    //write to sitemap.txt
    function writeSitemap() {
        var writePromise = new Promise(function(res, rej) {
            execute("echo '" + text + " ' > sitemap.txt", res, '');
        });
        return writePromise;
    }
    
    writeSitemap().then(function(data) {
        console.log(data);
    });

    
});

