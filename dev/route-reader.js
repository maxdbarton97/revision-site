var exec = require('child_process').exec;
var map = [];
var newScripts = [];

//run command using the string parameter
function execute(str, res) {
    exec(str, function(error, stdout, stderr) {
        //split the lines
        var data = stdout.split('\n');
        //remove the comma at the end
        data.splice(-1,1);
        //return promise
        res(data);
    });
}

function getFiles() {
    var filesPromise = new Promise(function(res, rej) {
        //get all files
        execute('cd app && ls -R', res);
    });
    return filesPromise;
}

function mapFilePaths(files) {
    var mapPromise = new Promise(function(res, rej) {
        //for all files
        for(var x=0; x < files.length; x++) {
            //if it's a file, not a test, not index.html/css/less,
            if (files[x].indexOf('.') > 0 && 
            files[x].indexOf('spec') < 0 &&
            files[x].indexOf('index.html') < 0 &&
            files[x].indexOf('index.css') < 0 &&
            files[x].indexOf('index.less') < 0
            ){
                //find the path by working way up to where path is listed
                var y = x - 1;
                while (true) {
                    //if at the path line
                    if (files[y].indexOf('.') === 0) {
                        //if not a path
                        if (files[y].indexOf('path') > 0) {
                            //push the path to map
                            map.push('app'+files[y].substring(1, files[y].length-1)+'/'+files[x]);
                            break;
                        } else {
                            break;
                        }
                    } else {
                        y -= 1;
                    }
                }
            }
        }
        //resolve
        res(true);
    });
    return mapPromise;
}

function getIndexPage() {
    var indexPromise = new Promise(function(res, rej) {
        //get document text
        execute('cat app/client/index.html', res);
    });
    return indexPromise;
}

function addScripts(lines) {
    var scriptPromise = new Promise(function(res, rej) {
        //go through each line and remove files from map if already listed
        //for every line in index.html
        for (var x=0; x < lines.length; x++) {
            //compare every map item with line
            for (var y=0; y < map.length; y++) {
                //if the file is listed in index.html
                if (lines[x].indexOf(map[y]) > 0) {
                    //remove it from the map array
                    map.splice(map.indexOf(map[y]), 1);
                }
            }
        }
        
        var template = "<script src='/'></script>";
        //for remaining files NOT listed
        for (var x=0; x < map.length; x++) {
            //if the map item is a js file
            if (map[x].indexOf('.js') > 0) {
                //it's new, so add it
                //compose string using template
                var script = [
                    template.slice(0, template.indexOf("/")+1),
                    map[x],
                    "'",
                    template.slice(template.indexOf('>'), template.length)
                ].join('');
                //compose command using sed command and script string to append to line 17
                var command = 'sed -i "17i' + script + '" app/client/index.html';
                //add the line
                exec(command);
            }
        }
        //resolve
        res(true);
    });
    return scriptPromise;
}

function getApp() {
    //get document text
    var appPromise = new Promise(function(res, rej) {
        //get document text
        execute('cat app/client/app.js', res);
    });
    return appPromise;
}

function addRoutes(lines) {
    var routePromise = new Promise(function(res, rej) {
        //remove app/client text from all routes
        for (var x=0; x < map.length; x++) {
            map[x] = map[x].slice(11, map[x].length);
        }
        //go through each line and remove files from map if already listed
        //for every line in app.js
        for (var x=0; x < lines.length; x++) {
            //compare every map item with line
            for (var y=0; y < map.length; y++) {
                //if the route is listed in app.js
                if (lines[x].indexOf(map[y]) > 0) {
                    //remove it from the map array
                    map.splice(map.indexOf(map[y]), 1);
                }
            }
        }
        
        //for routes not yet created for paths
        var template1 = ".when('/', {";
        var template2 = "templateUrl : '/'";
        
        //for remaining files NOT listed
        for (var x=0; x < map.length; x++) {
            //if the map item is a html file
            if (map[x].indexOf('.html') > 0) {
                //it's new, so add it
                //compose string using template
                //line1
                var path = map[x].slice(6, map[x].length);
                z = path.length-1;
                while (true) {
                    if (path[z] === '/') {
                        break;
                    } else {
                        z--;
                    }
                }
                temp1path = path.slice(0, z);
                //construct first line of path
                template1 = [
                    template1.slice(0, template1.indexOf('/')+1),
                    temp1path,
                    template1.slice(template1.length-4, template1.length)
                ].join('');
                //line2

                template2 = [
                    template2.slice(0, template2.indexOf('/')),
                    map[x],
                    template2.slice(template2.length-1, template2.length)
                ].join('');
                
                
                //compose command using sed command and route strings to append to line 21 sequentially
                var command1 = 'sed -i "21i   ' + template1 + '" app/client/app.js';
                var command2 = 'sed -i "21i       ' + template2 + '" app/client/app.js';
                var command3 = 'sed -i "21i   })" app/client/app.js';
                
                //execute composed command strings
                exec(command3 + ' && ' + command2 + ' && ' + command1);
                
            }
        }
        
        //resolve
        res(true);
    });
    return routePromise;
}

//promise chain 
getFiles()
    .then(mapFilePaths)
    .then(getIndexPage)
    .then(addScripts)
    .then(getApp)
    .then(addRoutes)
;